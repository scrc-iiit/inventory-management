const mongoose = require('mongoose');
const {API_BASE_URL} = require("./config");

// const mongoURI = config.API_BASE_URL;
mongoose.set('strictQuery', false); // Set strictQuery option to false to avoid deprecation warning

const connectToMongo = ()=>{
    mongoose.connect(API_BASE_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(()=>console.log('connected'))
    .catch(e=>console.log(e));
}

module.exports = connectToMongo;