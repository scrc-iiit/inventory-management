require("dotenv").config();

const config = {
  // API_BASE_URL: process.env.MONGODB_URL,
  // PORT: process.env.PORT
  API_BASE_URL: `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}`,
  // API_BASE_URL:"mongodb://localhost:27017/",

  // API_BASE_URL: "mongodb://localhost:27017/test",
  // API_BASE_URL: "mongodb+srv://root:toor@smartcity.qdwnwao.mongodb.net/inventory",
  JWT_SECRET: process.env.JWT || "#9006$#",
  PORT: process.env.PORT || 5000,
  auth_expire: process.env.AUTH_EXPIRE || "24h",
  SYSADMIN_USERNAME: process.env.SYSADMIN_USERNAME || "sysadmin",
  SYSADMIN_PASSWORD: process.env.SYSADMIN_PASSWORD || "sysadmin",
  SYSADMIN_EMAIL: process.env.SYSADMIN_EMAIL || "sysadmin@localhost",
};

module.exports = config;