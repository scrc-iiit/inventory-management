const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ItemSchema = new Schema({
  SKU: {
    type: String,
    required: true,
    ref: "Stock",
  },
  quantity: {
    type: Number,
    required: true,
  },
  purchase_date: {
    type: String,
    required: true,
  },
  purchased_by: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  store_name: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  purpose: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Item", ItemSchema);
