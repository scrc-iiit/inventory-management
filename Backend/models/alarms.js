const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;

// Helper function to get the current IST date and time
function getCurrentISTDate() {
  const now = new Date();
  const ISTOffset = 5.5 * 60 * 60 * 1000; // IST is UTC + 5 hours 30 minutes
  const ISTDate = new Date(now.getTime() + ISTOffset);
  return ISTDate;
}

const AlarmSchema = new Schema({
  id: {
    type: Number,
    unique: true,
  },
  createdAt: {
    type: Date,
    default:getCurrentISTDate, // Store IST time
  },
  purpose: {
    type: String,
    required: true,
  },
  parameter: {
    type: String,
    required: true,
  },
  stage: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  // name: {
  //   type: String,
  //   required: true,
  // },
});

// Apply the auto-increment plugin for the id field
AlarmSchema.plugin(AutoIncrement, { inc_field: 'id' });

module.exports = mongoose.model('Alarm', AlarmSchema);
