const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const DamagedSchema = new Schema({
//   SKU: {
//     type: String,
//     required: true,
//     ref: "Stock",
//   },
  name:{
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  cause: {
    type: String,
    required: true,
  },
  verify: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("damaged", DamagedSchema);
