const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Helper function to get the current IST date and time
function getCurrentISTDate() {
  const now = new Date();
  const ISTOffset = 5.5 * 60 * 60 * 1000; // IST is UTC + 5 hours 30 minutes
  const ISTDate = new Date(now.getTime() + ISTOffset);
  return ISTDate;
}


const StockSchema = new Schema({
  SKU: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  stocks: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: getCurrentISTDate, // Store IST time
  },
});
const Stock = mongoose.model("Stocks", StockSchema);
module.exports = Stock;
