const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Helper function to get the current IST date and time
function getCurrentISTDate() {
  const now = new Date();
  const ISTOffset = 0; // IST is UTC + 5 hours 30 minutes
  const ISTDate = new Date(now.getTime() + ISTOffset);
  return ISTDate;
}

const UsedItemSchema = new Schema({
  userID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  userName: {
    type: String,
    required: true,
  },
  SKU: {
    type: String,
    required: true,
    ref: "Stock",
  },
  purpose: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  Reported_by: {
    type: String,
  },
  usedQuantity: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: {
      values: ["Approved", "Rejected", "Pending"],
      message: "{VALUE} is not supported",
    },
    required: true,
  },
  rejectionRemarks: {
    type: String, // Field for storing rejection remarks
  },
  date: {
    type: Date,
    default: getCurrentISTDate, // Use the helper function to get the current IST date and time
  },
});

module.exports = mongoose.model("UsedItem", UsedItemSchema);
