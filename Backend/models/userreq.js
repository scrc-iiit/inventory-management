const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const URSchema = new Schema({
  userID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  userName: {
    type: String,
    required: true,
  },
  subject: {
    type: String,
    required: true,
  },
  body: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: {
      values: ["Approved", "Rejected", "Pending"],
      message: "{VALUE} is not supported",
    },
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Requirements", URSchema);
