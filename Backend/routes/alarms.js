const express = require("express");
const router = express.Router();
const Alarm = require("../models/alarms"); // Import Alarm model

// Route to get notification count (stage "N" and status "N/S")
router.get("/notification/count", async (req, res) => {
  try {
    const notificationCount = await Alarm.countDocuments({ stage: "N", status: 'N/S' });
    res.json({ count: notificationCount }); // Return count in JSON format
  } catch (error) {
    console.error("Error fetching notification count:", error);
    res.status(500).json({ error: "Error fetching notification count" });
  }
});

// Route to get notification data (stage "N" and status "N/S")
router.get("/notification/data", async (req, res) => {
  try {
    const notifications = await Alarm.find({ stage: "N", status: 'N/S'});
    res.json({
      success: true,
      notifications: notifications.length > 0 ? notifications : [],
    });
  } catch (error) {
    console.error("Error fetching notification data:", error);
    res.status(500).json({ error: "Error fetching notification data" });
  }
});

// Route to mark notifications as read (update stage "N" to "S")
router.put("/notification/mark-as-read/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const updatedNotification = await Alarm.findByIdAndUpdate(
      id,
      { status: "S" },
      { new: true } // Return the updated document
    );

    if (!updatedNotification) {
      return res.status(404).json({ error: "Notification not found" });
    }

    res.json({
      success: true,
      message: "Notification marked as read",
      notification: updatedNotification,
    });
  } catch (error) {
    console.error("Error marking notification as read:", error);
    res.status(500).json({ error: "Error marking notification as read" });
  }
});

// Route to get alarm count (stage "A" and status "N/S")
router.get("/alarm/count", async (req, res) => {
  try {
    const alarmCount = await Alarm.countDocuments({ stage: "A", status: "N/S" });
    res.json({ count: alarmCount }); // Return count in JSON format
  } catch (error) {
    console.error("Error fetching alarm count:", error);
    res.status(500).json({ error: "Error fetching alarm count" });
  }
});

// Route to get alarm data (stage "A" and status "N/S")
router.get("/alarm/data", async (req, res) => {
  try {
    const alarms = await Alarm.find({ stage: "A", status: "N/S" });
    res.json({
      success: true,
      alarms: alarms.length > 0 ? alarms : [],
    });
  } catch (error) {
    console.error("Error fetching alarm data:", error);
    res.status(500).json({ error: "Error fetching alarm data" });
  }
});

// Route to mark alarms as responded (update stage "A" to "S")
router.put("/mark-as-responded/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const updatedAlarm = await Alarm.findByIdAndUpdate(
      id,
      { status: "S" },
      { new: true } // Return the updated document
    );

    if (!updatedAlarm) {
      return res.status(404).json({ error: "Alarm not found" });
    }

    res.json({
      success: true,
      message: "Alarm marked as responded",
      alarm: updatedAlarm,
    });
  } catch (error) {
    console.error("Error marking alarm as responded:", error);
    res.status(500).json({ error: "Error marking alarm as responded" });
  }
});

module.exports = router;
