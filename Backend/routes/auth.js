const express = require("express");
const router = express.Router();
const User = require("../models/authentication");
const Alarm = require("../models/alarms"); // Assuming the Alarm model is in this path
const jwt = require("jsonwebtoken");
const fetchuser = require("../middleware/fetchuser");
const { JWT_SECRET, auth_expire } = require("../config");
const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");

// SMTP Configuration
const transporter = nodemailer.createTransport({
  host: "your-smtp-host",
  port: 587, // Change to 465 if using SSL
  secure: false, // Use true for 465, false for other ports
  auth: {
    user: "your-smtp-username",
    pass: "your-smtp-password",
  },
});

// Function to send email
const sendMail = async (to, subject, text) => {
  const mailOptions = {
    from: '"System Notifications" <your-email@example.com>', // Sender address
    to, // List of recipients
    subject, // Subject line
    text, // Plain text body
  };

  try {
    await transporter.sendMail(mailOptions);
    console.log(`Email sent to ${to} with subject: "${subject}"`);
  } catch (error) {
    console.error("Error sending email:", error.message);
  }
};

// Function to create a notification and send an email
const createNotification = async (parameter, purpose, stage = "N", status = "N/S") => {
  const alarm = new Alarm({ parameter, purpose, stage, status });
  await alarm.save();

  // Send an email notification
  await sendMail(
    "responsible-person@example.com", // Replace with the recipient's email
    purpose,
    `Notification Details:\n\nParameter: ${parameter}\nPurpose: ${purpose}\nStage: ${stage}\nStatus: ${status}`
  );
};

// Router1: Login function for both admin and user
router.post("/login", async (req, res) => {
  try {
    let user = await User.findOne({ username: req.body.username });
    if (!user) {
      return res.status(401).json({
        status: false,
        msg: "Please try to login with correct credentials",
      });
    }

    const passwordCompare = await bcrypt.compare(req.body.password, user.password);
    if (!passwordCompare) {
      return res.status(401).json({
        status: false,
        msg: "Please try to login with correct credentials",
      });
    }

    const data = {
      user: {
        id: user.id,
        username: user.username,
        role: user.role,
      },
    };

    const authtoken = jwt.sign(data, JWT_SECRET, { expiresIn: auth_expire });

    // Create login notification
    await createNotification("User Login", `User ${user.username} logged in`);

    res.json({ status: true, msg: "Login successful", authtoken });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router2: Create users by admin
router.post("/createcredentials", async (req, res) => {
  try {
    let user = await User.findOne({ username: req.body.username });
    if (user) {
      return res.status(401).json({
        status: false,
        msg: "Sorry, a user with this username already exists",
      });
    }

    const salt = await bcrypt.genSalt(5);
    const secPass = await bcrypt.hash(req.body.password, salt);

    user = await User.create({
      username: req.body.username,
      password: secPass,
      email: req.body.email,
      role: req.body.role,
    });

    // Create user creation notification
    await createNotification("User Creation", `User ${user.username} created by System Admin`);

    res.json({ status: true, msg: "Signup successful" });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router3: Delete user by admin
router.delete("/dprofile/:id", fetchuser, async (req, res) => {
  try {
    let user = await User.findById(req.params.id);
    if (!user) {
      return res.status(401).json({ status: false, msg: "User not found in database" });
    }

    const userrole = req.user.role;
    if (userrole === "user") {
      return res.status(500).json({ status: false, msg: "Access denied" });
    }

    await User.findByIdAndDelete(req.params.id);

    // Create user deletion notification
    await createNotification("User Deletion", `User ${user.username} deleted by ${userrole}`);

    res.json({ status: true, msg: "User credentials have been deleted", user });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router4: Update user/admin profile
router.put("/uprofile/:id", fetchuser, async (req, res) => {
  const { role, password } = req.body;

  try {
    const userrole = req.user.role;
    let userpro = await User.findById(req.params.id);
    if (!userpro) {
      return res.status(404).json({ status: false, msg: "User not found in database" });
    }

    if (userrole === "user") {
      return res.status(500).json({ status: false, msg: "Access denied" });
    }

    const newNote = {};
    let updateDetails = [];

    if (password !== "") {
      const salt = await bcrypt.genSalt(5);
      const secPass = await bcrypt.hash(password, salt);
      newNote.password = secPass;
      updateDetails.push("password");
    }

    if (role) {
      newNote.role = role;
      updateDetails.push("role");
    }

    userpro = await User.findByIdAndUpdate(req.params.id, { $set: newNote }, { new: true });

    // Create update notification
    let roleChange = updateDetails.includes("role") ? ` as ${role}` : '';
    await createNotification("User Update", `User ${userpro.username} updated by ${userrole}${roleChange}`);

    res.json({ status: true, msg: "Update successful", userpro });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router5: User profile view
router.get("/getuser", fetchuser, async (req, res) => {
  try {
    const userId = req.user.id;
    const user = await User.findById(userId).select(["-password", "-date"]);
    res.send(user);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router6: Admin view all user profiles
router.get("/getalluser", fetchuser, async (req, res) => {
  try {
    const userrole = req.user.role;
    if (userrole === "user") {
      return res.status(500).json({ status: false, msg: "Access denied" });
    }
    const users = await User.find();
    res.send(users);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router7: Change password
router.put('/changepassword', fetchuser, async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  try {
    let userpro = await User.findById(req.user.id);
    if (!userpro) {
      return res.status(404).json({ status: false, msg: "User not found in database" });
    }
    const passwordCompare = await bcrypt.compare(oldPassword, userpro.password);
    if (!passwordCompare) {
      return res.status(401).json({
        status: false,
        msg: "Invalid existing password! Please enter the correct password",
      });
    }

    const salt = await bcrypt.genSalt(5);
    const secPass = await bcrypt.hash(newPassword, salt);

    userpro = await User.findByIdAndUpdate(req.user.id, { $set: { password: secPass } }, { new: true });

    // Create password change notification
    await createNotification("Password Change", `User ${userpro.username} has changed their password`);

    res.json({ status: true, msg: "Password updated successfully", userpro });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

module.exports = router;
