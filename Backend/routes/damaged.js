const express = require("express");
const router = express.Router();
const fetchuser = require("../middleware/fetchuser");
const DamagedSchema = require("../models/damaged");
const Alarm = require("../models/alarms");
const nodemailer = require("nodemailer");

// SMTP Configuration
const transporter = nodemailer.createTransport({
  host: "your-smtp-host",
  port: 587, // Change to 465 if using SSL
  secure: false, // Use true for 465, false for other ports
  auth: {
    user: "your-smtp-username",
    pass: "your-smtp-password",
  },
});

// Function to send email
const sendMail = async (to, subject, text) => {
  const mailOptions = {
    from: '"System Notifications" <your-email@example.com>', // Sender address
    to, // List of recipients
    subject, // Subject line
    text, // Plain text body
  };

  try {
    await transporter.sendMail(mailOptions);
    console.log(`Email sent to ${to} with subject: "${subject}"`);
  } catch (error) {
    console.error("Error sending email:", error.message);
  }
};

// Route to add damaged item and raise alarm notification
router.post("/request", fetchuser, async (req, res) => {
  try {
    if (req.user.role === "user") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }

    const Drequest = new DamagedSchema({
      name: req.body.name,
      quantity: req.body.quantity,
      cause: req.body.cause,
      verify: req.body.verify,
    });

    const savedDRS = await Drequest.save();

    const purposeMessage = `${req.body.quantity} damaged items added by ${req.user.role} that are ${req.body.cause}`;

    const alarm = new Alarm({
      parameter: req.body.name,
      purpose: purposeMessage,
      stage: "N",
      status: "N/S",
    });

    await alarm.save();

    // Send email notification
    await sendMail(
      "responsible-person@example.com", // Replace with recipient's email
      purposeMessage,
      `Notification Details:\n\nParameter: ${req.body.name}\nQuantity: ${req.body.quantity}\nCause: ${req.body.cause}\nStatus: ${alarm.status}`
    );

    return res.json({
      status: true,
      msg: "Added successfully",
      savedDRS,
      alarm: {
        msg: "Notification raised for damaged item addition",
        itemName: alarm.parameter,
        status: alarm.status,
        stage: alarm.stage,
        purpose: alarm.purpose,
      },
    });
  } catch (error) {
    console.error("Error adding damaged request:", error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route to fetch all damaged requests (admin only)
router.get("/getall", fetchuser, async (req, res) => {
  try {
    if (req.user.role === "user") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }

    const data = await DamagedSchema.find();
    res.json(data);
  } catch (err) {
    console.error("Error fetching all damaged requests:", err.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route to delete a damaged item (admin only)
router.delete("/delete/:id", fetchuser, async (req, res) => {
  try {
    if (req.user.role === "user") {
      return res.status(500).json({ status: false, msg: "Access denied" });
    }

    let data = await DamagedSchema.findById(req.params.id);
    if (!data) {
      return res.status(401).json({ status: false, msg: "Damaged item not found in database" });
    }

    data = await DamagedSchema.findByIdAndDelete(req.params.id);

    // Send email notification for deletion
    await sendMail(
      "responsible-person@example.com", // Replace with recipient's email
      "Damaged Item Deletion",
      `The damaged item "${data.name}" has been deleted from the database.`
    );

    res.json({ status: true, msg: "Request has been deleted", item: data });
  } catch (error) {
    console.error("Error deleting damaged item:", error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route to update verification status of a damaged item (admin only)
router.put("/update/:id", fetchuser, async (req, res) => {
  try {
    const { verify } = req.body;

    if (req.user.role === "user") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }

    let damagedItem = await DamagedSchema.findById(req.params.id);
    if (!damagedItem) {
      return res.status(401).json({ status: false, msg: "Damaged item not found in database" });
    }

    const data = await DamagedSchema.findByIdAndUpdate(
      req.params.id,
      { verify: verify },
      { new: true }
    );

    // Send email notification for update
    await sendMail(
      "responsible-person@example.com", // Replace with recipient's email
      "Verification Status Updated",
      `The verification status of "${damagedItem.name}" has been updated to "${verify}".`
    );

    return res.json({ status: true, msg: "Verified", updatedItem: data });
  } catch (err) {
    console.error("Error updating verification status:", err.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

module.exports = router;
