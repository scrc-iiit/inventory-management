const express = require("express");
const router = express.Router();
const fetchuser = require("../middleware/fetchuser");
const Item = require("../models/Item");
const Stock = require("../models/stock");
const Alarm = require("../models/alarms");
const cron = require("node-cron");
const nodemailer = require("nodemailer");

// Store the timestamp of the last added item
let lastAddedTime = null;

// SMTP Configuration for Email Notifications
const transporter = nodemailer.createTransport({
  host: "your-smtp-host",
  port: 587, // Change to 465 if using SSL
  secure: false, // Use true for 465, false for other ports
  auth: {
    user: "your-smtp-username",
    pass: "your-smtp-password",
  },
});

// Function to send email
const sendMail = async (to, subject, text) => {
  const mailOptions = {
    from: '"System Notifications" <your-email@example.com>',
    to, // Recipient's email address
    subject, // Email subject
    text, // Email body
  };

  try {
    await transporter.sendMail(mailOptions);
    console.log(`Email sent to ${to} with subject: "${subject}"`);
  } catch (error) {
    console.error("Error sending email:", error.message);
  }
};

// Cron job that runs every one day to check if a new item has been added
cron.schedule("0 0 * * *", async () => {
  try {
    if (lastAddedTime && (Date.now() - lastAddedTime) > 86400000) {
      const purposeMessage = "No components were added for more than 24 hours.";
      const newAlarm = new Alarm({
        parameter: "System",
        purpose: purposeMessage,
        stage: "N",
        status: "N/S",
      });
      await newAlarm.save();

      // Send email notification
      await sendMail(
        "responsible-person@example.com",
        purposeMessage,
        "This is to notify that no components have been added in the system for over 24 hours."
      );

      console.log("Raised notification due to inactivity of 24 hours");
    }
  } catch (error) {
    console.error("Error checking inactivity:", error.message);
  }
});

// Router for admin to add components and information
router.post("/add", fetchuser, async (req, res) => {
  try {
    const userrole = req.user.role;
    if (userrole === "user") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }

    const { sku, quantity, purchase_date, store_name, price, purpose } = req.body;
    if (!sku || !quantity || !purchase_date || !store_name || !price || !purpose) {
      return res.status(400).json({ status: false, msg: "Please provide all required fields" });
    }

    const stock = await Stock.findOne({ SKU: sku });
    if (!stock) {
      return res.status(404).json({ status: false, msg: "Stock not found in the database" });
    }

    const Updatestock = await Stock.findOneAndUpdate(
      { SKU: sku },
      { $inc: { stocks: +quantity } },
      { new: true }
    );

    const newItem = new Item({
      SKU: sku,
      quantity,
      purchase_date,
      purchased_by: req.user.username,
      price,
      store_name,
      purpose,
    });
    const savedItem = await newItem.save();

    lastAddedTime = Date.now();

    const purposeMessage = `${quantity} components purchased by ${req.user.username}`;
    const newAlarm = new Alarm({
      parameter: sku,
      purpose: purposeMessage,
      stage: "N",
      status: "N/S",
    });
    await newAlarm.save();

    // Send email notification
    await sendMail(
      "responsible-person@example.com",
      purposeMessage,
      `Details:\n\nSKU: ${sku}\nQuantity: ${quantity}\nStore Name: ${store_name}\nPurpose: ${purpose}`
    );

    res.json({ status: true, msg: "Successfully added data", savedItem, Updatestock });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router for admin to fetch all components information
router.get("/getall", fetchuser, async (req, res) => {
  try {
    const userrole = req.user.role;
    if (userrole === "user") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }
    const items = await Item.find();
    res.json(items);
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Router for admin to delete item information
router.delete("/delete/:id", fetchuser, async (req, res) => {
  try {
    const userrole = req.user.role;
    if (userrole === "user") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }

    let data = await Item.findById(req.params.id);
    if (!data) {
      return res.status(404).json({ status: false, msg: "Data not found in the database" });
    }

    const Updatestock = await Stock.findOneAndUpdate(
      { SKU: data.SKU },
      { $inc: { stocks: -data.quantity } },
      { new: true }
    );

    await Item.findByIdAndDelete(req.params.id);

    const purposeMessage = `Deleted item with SKU ${data.SKU} and quantity ${data.quantity}`;
    const newAlarm = new Alarm({
      parameter: data.SKU,
      purpose: purposeMessage,
      stage: "N",
      status: "N/S",
    });
    await newAlarm.save();

    // Send email notification
    await sendMail(
      "responsible-person@example.com",
      purposeMessage,
      `Details:\n\nSKU: ${data.SKU}\nQuantity: ${data.quantity} has been removed.`
    );

    res.json({ status: true, msg: "Deleted successfully", Updatestock });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

module.exports = router;
