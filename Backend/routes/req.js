const express = require("express");
const router = express.Router();
const fetchuser = require("../middleware/fetchuser");
const URS = require("../models/userreq");
const Alarm = require("../models/alarms");
const nodemailer = require("nodemailer");

// SMTP Configuration for Email Notifications
const transporter = nodemailer.createTransport({
  host: "your-smtp-host",  // SMTP host (e.g., smtp.gmail.com for Gmail)
  port: 587,  // Use 465 for SSL or 587 for TLS
  secure: false,  // Set to true for port 465, false for 587
  auth: {
    user: "your-smtp-username",  // SMTP username (e.g., your Gmail address)
    pass: "your-smtp-password",  // SMTP password
  },
});

// Function to send email
const sendMail = async (to, subject, text) => {
  const mailOptions = {
    from: '"System Notifications" <your-email@example.com>', // Sender address
    to,  // Receiver's email address
    subject,  // Subject of the email
    text,  // Plain text body
  };

  try {
    await transporter.sendMail(mailOptions);
    console.log(`Email sent to ${to} with subject: "${subject}"`);
  } catch (error) {
    console.error("Error sending email:", error.message);
  }
};

// Route for user to create a request and raise notification
router.post("/request", fetchuser, async (req, res) => {
  try {
    // Check if the user is authorized to raise a request
    if (req.user.role !== "user") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }

    // Create a new user request
    const userRequest = new URS({
      userID: req.user.id,
      userName: req.user.username,
      subject: req.body.subject,
      body: req.body.body,
      status: "Pending",
      createdAt: new Date(), // Timestamp the creation
    });

    const savedRequest = await userRequest.save();
    console.log("New user request created:", savedRequest);

    // Raising a notification (alarm) with stage 'N' and status 'N/S'
    const purposeMessage = `New request raised by ${req.user.username}`;
    const newAlarm = new Alarm({
      parameter: req.user.id,   // User ID as the parameter
      purpose: purposeMessage,  // Purpose describes who raised the request
      stage: "N",               // Initial stage set to 'N'
      status: "N/S",            // Status set to 'N/S'
    });

    const savedAlarm = await newAlarm.save();
    console.log("Notification raised and stored in alarms collection:", savedAlarm);

    // Send email notification
    await sendMail(
      "admin@example.com",  // Replace with the admin email
      "New Request Raised",
      `New request raised by ${req.user.username} with the following details:\n\nSubject: ${req.body.subject}\nBody: ${req.body.body}`
    );

    return res.json({
      status: true,
      msg: "Request created successfully. Wait for Admin response.",
      savedRequest,
      alarm: {
        msg: "Notification raised for new user request",
        status: savedAlarm.status,
        stage: savedAlarm.stage,
      },
    });
  } catch (error) {
    console.error("Error creating user request and raising notification:", error.message);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route to fetch all requests for admins and specific requests for users
router.get("/getall", fetchuser, async (req, res) => {
  try {
    let data;
    if (req.user.role === "admin") {
      // Admin can see all requests
      data = await URS.find();
    } else {
      // Users can only see their own requests
      data = await URS.find({ userName: req.user.username });
    }
    return res.json(data);
  } catch (error) {
    console.error("Error fetching requests:", error.message);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route for admin to approve/reject requests
router.put("/update/:id", fetchuser, async (req, res) => {
  try {
    // Only admins can update requests
    if (req.user.role !== "admin") {
      return res.status(401).json({ status: false, msg: "You can't access this" });
    }

    const request = await URS.findById(req.params.id);

    // Prevent updates to already approved/rejected requests
    if (request.status === "Approved" || request.status === "Rejected") {
      return res.status(401).json({ status: false, msg: "This request has already been processed" });
    }

    const { status } = req.body;
    if (status === "Approved" || status === "Rejected") {
      request.status = status;
      const updatedRequest = await request.save();
      console.log(`Request ${req.params.id} updated to ${status}`);

      // Send email notification to the user about request status update
      await sendMail(
        request.userName,  // Send the email to the user who made the request
        "Your Request Status Update",
        `Your request with subject: "${request.subject}" has been updated to: ${status}.`
      );

      return res.json({ status: true, msg: `${status} Successfully`, updatedRequest });
    }

    return res.status(400).json({ status: false, msg: "Invalid status provided" });
  } catch (error) {
    console.error("Error updating request:", error.message);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Background job to check pending requests and raise alarms
setInterval(async () => {
  try {
    // Get the current timestamp
    const now = new Date();

    // Fetch all requests that are still pending
    const pendingRequests = await URS.find({ status: "Pending" });

    // Loop through all pending requests
    pendingRequests.forEach(async (request) => {
      // Calculate the time elapsed since the request was created (in seconds)
      const timeElapsed = (now.getTime() - new Date(request.createdAt).getTime()) / 1000;

      // Check if the request has been pending for more than 7200 seconds (2 hours)
      if (timeElapsed > 7200) {
        console.log(`Request ${request._id} has been pending for ${timeElapsed} seconds`);

        // Check if an alarm for this request already exists (optional)
        const existingAlarm = await Alarm.findOne({ purpose: `Request ${request._id} pending too long` });
        if (existingAlarm) {
          console.log(`Alarm already exists for request ${request._id}`);
          return;
        }

        // Raise an alarm and store it in the alarms collection
        const alarm = new Alarm({
          parameter: "Request Timeout",
          purpose: `Request ${request._id} pending too long`,
          stage: "A",  // Stage 'A' indicates alarm raised
          status: "N/S",
        });

        await alarm.save();
        console.log(`Alarm raised for request ${request._id} and stored in alarms collection`);

        // Send email notification for the alarm
        await sendMail(
          "admin@example.com",  // Replace with the admin email
          `Alarm: Request ${request._id} Pending Too Long`,
          `The request with ID: ${request._id} has been pending for more than 2 hours. Please take action.`
        );
      }
    });
  } catch (error) {
    console.error("Error checking pending requests for alarms:", error.message);
  }
}, 500000); // Run every 5 seconds to check pending requests

module.exports = router;
