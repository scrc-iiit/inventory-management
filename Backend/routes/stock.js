const express = require("express");
const router = express.Router();
const cron = require("node-cron");
const fetchuser = require("../middleware/fetchuser");
const Stock = require("../models/stock");
const Alarm = require("../models/alarms");
const nodemailer = require("nodemailer");

// SMTP Configuration for Email Notifications
const transporter = nodemailer.createTransport({
  host: "your-smtp-host",  // SMTP host (e.g., smtp.gmail.com for Gmail)
  port: 587,  // Use 465 for SSL or 587 for TLS
  secure: false,  // Set to true for port 465, false for 587
  auth: {
    user: "your-smtp-username",  // SMTP username (e.g., your Gmail address)
    pass: "your-smtp-password",  // SMTP password
  },
});

// Function to send email
const sendMail = async (to, subject, text) => {
  const mailOptions = {
    from: '"Inventory System" <your-email@example.com>', // Sender address
    to,  // Receiver's email address
    subject,  // Subject of the email
    text,  // Plain text body
  };

  try {
    await transporter.sendMail(mailOptions);
    console.log(`Email sent to ${to} with subject: "${subject}"`);
  } catch (error) {
    console.error("Error sending email:", error.message);
  }
};

// Function to pad SKU numbers with leading zeros
function padNumber(num) {
  return "sku" + num.toString().padStart(4, "0");
}

// Route to generate SKU and add new stock item with alarm notification
router.post("/generate", fetchuser, async (req, res) => {
  try {
    const userrole = req.user.role;
    if (userrole === "user") {
      return res.status(403).json({ status: false, msg: "Access denied" });
    }

    const { name, location } = req.body;

    // Find the highest SKU number and increment from there to avoid duplicates
    const lastStock = await Stock.findOne().sort({ SKU: -1 });
    const lastSKU = lastStock ? parseInt(lastStock.SKU.replace("sku", "")) : 0;
    const sku = padNumber(lastSKU + 1);

    const now = new Date();
    const ISTOffset = 5.5 * 60 * 60 * 1000;
    const createdAtIST = new Date(now.getTime() + ISTOffset);

    const stock = new Stock({
      SKU: sku,
      name: name,
      location: location,
      createdAt: createdAtIST,
    });
    const savedStock = await stock.save();

    const purpose = `A new SKU has been created with the following details:

    SKU: ${sku}
    Name: ${name}
    Location: ${location}`;

    const alarm = new Alarm({
      parameter: sku,
      purpose: purpose,
      stage: "N",
      status: "N/S",
    });
    await alarm.save();

    // Send email notification to admin
    await sendMail(
      "admin@example.com",  // Replace with the admin email
      "New SKU Created",
      `A new SKU has been created with the following details:\n\nSKU: ${sku}\nName: ${name}\nLocation: ${location}`
    );

    return res.json({
      status: true,
      msg: "Generated successfully",
      savedStock,
      alarm: {
        msg: "Notification raised for SKU creation",
        SKU: alarm.parameter,
        status: alarm.status,
        stage: alarm.stage,
      },
    });
  } catch (error) {
    console.error(error.message);
    return res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route to fetch all stock items
router.get("/getall", async (req, res) => {
  try {
    const data = await Stock.find();
    return res.json(data);
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route to get stock count and breakdown of stock statuses
router.get("/count", async (req, res) => {
  try {
    const totalCount = await Stock.countDocuments();
    const zeroCount = await Stock.countDocuments({ stocks: 0 });
    const gt5Count = await Stock.countDocuments({ stocks: { $gt: 0 } });
    const data = [
      { name: "Total Components", value: totalCount, code: "total" },
      { name: "Out of Stocks", value: zeroCount, total: totalCount, code: "out" },
      { name: "In Stocks", value: gt5Count, total: totalCount, code: "in" },
    ];
    return res.json({ msg: "Welcome to Inventory", data });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Route to update stock information
router.put("/update/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const updatedStock = await Stock.findByIdAndUpdate(
      id,
      { $set: req.body },
      { new: true }
    );

    if (!updatedStock) {
      return res.status(404).json({ msg: "Stock item not found" });
    }

    // Send email notification to admin about the stock update
    await sendMail(
      "admin@example.com",  // Replace with the admin email
      "Stock Updated",
      `The stock with SKU: ${updatedStock.SKU} has been updated. Details:\n\nName: ${updatedStock.name}\nLocation: ${updatedStock.location}\nUpdated fields: ${JSON.stringify(req.body)}`
    );

    res.json({ status: true, msg: "Stock updated successfully", updatedStock });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Scheduled task to check stock levels every day
cron.schedule("0 0 * * *", async () => {
  try {
    const stocks = await Stock.find();

    for (const item of stocks) {
      const stockQuantity = item.stocks; // Assuming `stocks` field stores the quantity

      if (stockQuantity < 5) {
        const existingAlarm = await Alarm.findOne({
          parameter: item.SKU,
          purpose: `Critical Stock Alert for ${item.name}: ${stockQuantity}`,
          stage: "A",
        });
        if (!existingAlarm) {
          const alarm = new Alarm({
            parameter: item.SKU,
            purpose: `Critical Stock Alert for ${item.name}: ${stockQuantity}`,
            stage: "A",
            status: "N/S",
          });
          await alarm.save();
          console.log(`Alarm raised for SKU: ${item.SKU} - Critical Stock Alert`);

          // Send email notification for critical stock
          await sendMail(
            "admin@example.com",  // Replace with the admin email
            `Critical Stock Alert: ${item.name}`,
            `The stock for SKU: ${item.SKU} is critically low (${stockQuantity} left). Please take action.`
          );
        }
      } else if (stockQuantity < 10) {
        const existingNotification = await Alarm.findOne({
          parameter: item.SKU,
          purpose: `Low Stock Notification for ${item.name}: ${stockQuantity}`,
          stage: "N",
        });
        if (!existingNotification) {
          const notification = new Alarm({
            parameter: item.SKU,
            purpose: `Low Stock Notification for ${item.name}: ${stockQuantity}`,
            stage: "N",
            status: "N/S",
          });
          await notification.save();
          console.log(`Notification raised for SKU: ${item.SKU} - Low Stock Notification`);

          // Send email notification for low stock
          await sendMail(
            "admin@example.com",  // Replace with the admin email
            `Low Stock Notification: ${item.name}`,
            `The stock for SKU: ${item.SKU} is low (${stockQuantity} left). Please consider restocking soon.`
          );
        }
      }
    }
  } catch (error) {
    console.error("Error in scheduled stock check:", error.message);
  }
});

module.exports = router;
