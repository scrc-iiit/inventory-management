const express = require("express");
const router = express.Router();
const fetchuser = require("../middleware/fetchuser");
const useditem = require("../models/useditem");
const Stock = require("../models/stock");
const Alarm = require("../models/alarms");
const nodemailer = require("nodemailer");

// SMTP configuration for email (using Gmail as an example)
const transporter = nodemailer.createTransport({
  service: 'gmail',  // Use your SMTP service provider
  auth: {
    user: 'your-email@gmail.com',  // Replace with your email
    pass: 'your-email-password',   // Replace with your email password or app password
  },
});

// Helper function to send an email
const sendEmail = (subject, message, toEmail = "recipient@example.com") => {
  const mailOptions = {
    from: 'your-email@gmail.com',  // Your email
    to: toEmail,                   // Recipient's email
    subject: subject,              // Subject (purpose of the alarm)
    text: message,                 // The body of the email
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log("Error sending email:", error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
};

// Helper function to get the current IST date and time
function getCurrentISTDate() {
  const now = new Date();
  const ISTOffset = 0; // IST is UTC + 5 hours 30 minutes
  const ISTDate = new Date(now.getTime() + ISTOffset);
  return ISTDate;
}

// Creating the request
router.post("/create", fetchuser, async (req, res) => {
  try {
    const { usedQuantity, purpose, sku } = req.body;

    // Checking if stock exists
    const stock = await Stock.findOne({ SKU: sku });
    if (!stock) {
      return res.status(404).json({ status: false, msg: "Stock not found in the database" });
    }

    // Create the used item data
    const data = new useditem({
      userID: req.user.id,
      userName: req.user.username,
      SKU: sku,
      purpose,
      name: stock.name, // Set the name from the stock data
      usedQuantity,
      status: req.user.role === "sysadmin" ? "Approved" : "Pending",
      date: getCurrentISTDate(), // Use IST for timestamp
      Reported_by: req.user.role === "sysadmin" ? req.user.username : null,
    });

    // Save the used item
    const savedUsedItem = await data.save();

    // Raise a notification instantly when a request is created
    const notification = new Alarm({
      parameter: sku,
      purpose,
      stage: "N",
      status: "N/S",
      createdAt: getCurrentISTDate(),
    });
    await notification.save();

    // Send an email notification for the new request
    sendEmail(purpose, `A new used item request was created for SKU: ${sku}.`, "recipient@example.com");

    // Update stock if sysadmin
    if (req.user.role === "sysadmin") {
      await Stock.findOneAndUpdate(
        { SKU: sku },
        { $inc: { stocks: -usedQuantity } }, // Decrementing the quantity
        { new: true }
      );
    }

    return res.json({ status: true, msg: "Request created successfully", savedUsedItem });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Fetch all used items
router.get("/getall", fetchuser, async (req, res) => {
  try {
    const items = req.user.role === "user" 
      ? await useditem.find({ userID: req.user.id }) 
      : await useditem.find();
    
    return res.json(items);
  } catch (err) {
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Update the admin response (Approve/Reject)
router.put("/update/:id", fetchuser, async (req, res) => {
  try {
    const { status, remarks } = req.body;

    // Ensure status is provided
    if (!status) {
      return res.status(400).json({ msg: "Status is required" });
    }

    // Find the used item by ID
    const usedItem = await useditem.findById(req.params.id);
    if (!usedItem) {
      return res.status(404).json({ status: false, msg: "Data not found in database" });
    }

    // Prevent overwriting an existing status if it’s already set to Approved or Rejected
    if (usedItem.status === "Approved" || usedItem.status === "Rejected") {
      return res.status(400).json({ status: false, msg: "Overwriting existing status is not allowed" });
    }

    // Prepare the fields to update based on status
    const updateData = {
      status,
      Reported_by: req.user.username, // Set the user who made the update
      rejectionRemarks: status === "Rejected" ? remarks : null, // Set remarks only for rejections
    };

    // Update the used item and return the modified item
    const updatedItem = await useditem.findByIdAndUpdate(req.params.id, updateData, { new: true });

    // Send an email notification when status is updated
    sendEmail(`Used Item Request ${status}`, `The request for SKU: ${usedItem.SKU} has been ${status}.`, "recipient@example.com");

    return res.json({
      status: true,
      msg: status === "Approved" ? "Request approved successfully" : "Request rejected successfully",
      savedUsedItem: updatedItem,
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "An error occurred while updating the request." });
  }
});

// Delete used item request
router.delete("/delete/:id", fetchuser, async (req, res) => {
  try {
    const data = await useditem.findById(req.params.id);
    if (!data) {
      return res.status(404).json({ status: false, msg: "Data not found" });
    }

    if (data.status === "Approved") {
      const qu = data.usedQuantity;
      await Stock.findOneAndUpdate(
        { SKU: data.SKU },
        { $inc: { stocks: +qu } }, // Increment the quantity back to stock
        { new: true }
      );
    }

    await useditem.findByIdAndDelete(req.params.id);
    res.json({ status: true, msg: "Deleted successfully" });
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Update request log (used item data) if needed
router.put("/updateRequest/:id", fetchuser, async (req, res) => {
  try {
    const { usedQuantity, purpose, status } = req.body;

    const usedItem = await useditem.findById(req.params.id);
    if (!usedItem) {
      return res.status(404).json({ status: false, msg: "Request log not found" });
    }

    if (usedItem.status !== "Pending") {
      return res.status(400).json({ status: false, msg: "Cannot update log that is not pending" });
    }

    // Only admins or sysadmins should be able to modify the log
    if (req.user.role === "admin" || req.user.role === "sysadmin") {
      let updateFields = {};

      if (usedQuantity) updateFields.usedQuantity = usedQuantity;
      if (purpose) updateFields.purpose = purpose;
      if (status) updateFields.status = status;

      const updatedRequest = await useditem.findByIdAndUpdate(
        req.params.id,
        { $set: updateFields },
        { new: true }
      );

      // Send an email notification when request is updated
      sendEmail(`Used Item Request Updated`, `The request for SKU: ${usedItem.SKU} has been updated.`, "recipient@example.com");

      return res.json({
        status: true,
        msg: "Request log updated successfully",
        updatedRequest,
      });
    } else {
      return res.status(403).json({ status: false, msg: "Unauthorized action" });
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ status: false, msg: "Internal Server Error" });
  }
});

// Alarm and notification trigger logic
setInterval(async () => {
  try {
    const nowIST = getCurrentISTDate();
    const twohoursAgoIST = new Date(nowIST.getTime() - 2 * 60 * 60 * 1000); // 2 hours ago in IST

    // Find requests in "Pending" status created more than 2 hours ago
    const pendingItems = await useditem.find({
      status: "Pending",
      date: { $lt: twohoursAgoIST }, // Check using IST time
    });

    for (const item of pendingItems) {
      // Create a new alarm for pending requests
      const alarm = new Alarm({
        parameter: item.SKU,
        purpose: item.purpose,
        stage: "A",
        status: "N/S",
        createdAt: getCurrentISTDate(),
      });
      await alarm.save();

      // Send an email when an alarm is raised
      sendEmail(`Pending Request Alert`, `The request for SKU: ${item.SKU} has been pending for more than 2 hours.`, "recipient@example.com");
      
      console.log(`Alarm raised for pending request with SKU: ${item.SKU}`);
    }
  } catch (error) {
    console.error("Error in alarm trigger:", error.message);
  }
}, 2 * 60 * 60 * 1000); // Check every 2 hours

module.exports = router;
