const express = require("express");
const cors = require("cors");
const { PORT } = require("./config");
const Alarm = require("./models/alarms"); // Alarm model
const URS = require("./models/userreq"); // User request model
const Stock = require("./models/stock"); // Stock model to fetch component names
const connectToMongo = require("./Database");
const cron = require("node-cron"); // For scheduling tasks
const alarmRoutes = require("./routes/alarms");
const createSysAdmin = require("./setup/initializeUser")

// Connect to MongoDB
connectToMongo();


// Create the sysadmin user if it doesn't exist
createSysAdmin();

const app = express();
const port = PORT || 5000;

// Configure CORS
const corsOptions = {
  origin: "*",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  credentials: true,
};

app.use(cors(corsOptions));
app.use(express.json());

// Routes
app.use("/api/auth", require("./routes/auth"));
app.use("/api/stock", require("./routes/stock"));
app.use("/api/ticket", require("./routes/req"));
app.use("/api/store/purchase", require("./routes/purstock"));
app.use("/api/store/use", require("./routes/usestock"));
app.use("/api/damaged", require("./routes/damaged"));
app.use("/api/alarm", alarmRoutes);

// Function to raise alarms for pending requests older than 1.5 hours
const raisePendingAlarms = async () => {
  try {
    console.log("Checking for pending requests older than 1.5 hours...");

    const thresholdTime = new Date(Date.now() - (1 * 60 + 30) * 60 * 1000); // 1.5 hours in milliseconds

    const pendingRequests = await URS.find({
      status: "Pending",
      createdAt: { $lte: thresholdTime },
    });

    if (pendingRequests.length > 0) {
      console.log(`Found ${pendingRequests.length} pending requests older than 1.5 hours.`);

      for (const request of pendingRequests) {
        // Fetch the stock component name
        const stockItem = await Stock.findOne({ SKU: request.SKU });

        if (!stockItem) {
          console.log(`Stock item not found for SKU: ${request.SKU}`);
          continue; // Skip if no stock item is found
        }

        // Construct the purpose message
        const purposeMessage = `Request for ${stockItem.name} for ${request.userName} is pending for over 1.5 hours`;

        // Check if an alarm already exists for this request
        const existingAlarm = await Alarm.findOne({
          parameter: request.userID,
          purpose: purposeMessage,
          stage: "A",
          status: "N/S",
        });

        if (!existingAlarm) {
          // Create a new alarm
          const newAlarm = new Alarm({
            parameter: request.userID,
            purpose: purposeMessage,
            name: stockItem.name,
            stage: "A",
            status: "N/S",
            createdAt: new Date(),
          });

          await newAlarm.save();
          console.log(`New alarm raised: ${purposeMessage}, Request ID: ${request._id}`);
        } else {
          console.log(`Alarm already exists for request ID: ${request._id}, User: ${request.userName}`);
        }
      }
    } else {
      console.log("No pending requests older than 1.5 hours found.");
    }

    // Fetch and log alarms (stage "A") and notifications (stage "N")
    const alarms = await Alarm.find({ stage: "A" });
    const notifications = await Alarm.find({ stage: "N" });

    console.log(`Total alarms: ${alarms.length}`);
    console.log("Alarms (JSON format):", JSON.stringify(alarms, null, 2));

    console.log(`Total notifications: ${notifications.length}`);
    console.log("Notifications (JSON format):", JSON.stringify(notifications, null, 2));
  } catch (error) {
    console.error("Error raising alarms for pending requests:", error.message);
  }
};

// Cron job to run every 1.5 hours
cron.schedule("30 */1 * * *", async () => {
  console.log("Running cron job for checking pending requests...");
  await raisePendingAlarms();
});

// Initial check for alarms and notifications
(async () => {
  console.log("Initial check for existing alarms and pending requests");
  await raisePendingAlarms();
})();

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
