const User = require("../models/authentication");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const {
  SYSADMIN_USERNAME,
  SYSADMIN_PASSWORD,
  SYSADMIN_EMAIL,
} = require("../config");

const createSysAdmin = async () => {
  try {
    // Check if the sysadmin user already exists in the database
    let sysadmin = await User.findOne({ username: SYSADMIN_USERNAME });
    if (sysadmin) {
      console.log("sysadmin already exists");
      return;
    }

    // Hash and salt the password
    const salt = await bcrypt.genSalt(5);
    const secPass = await bcrypt.hash(SYSADMIN_PASSWORD, salt);

    // Create the sysadmin user
    sysadmin = await User.create({
      username: SYSADMIN_USERNAME,
      password: secPass,
      email: SYSADMIN_EMAIL,
      role: "sysadmin",
    });

    console.log("sysadmin created successfully");
  } catch (error) {
    console.error("Error creating sysadmin:", error.message);
  }
};
module.exports = createSysAdmin;
