import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AuthState from "./context/auth/AuthState";
import Login from "./components/Login";
import Dashboard from "./Pages/Dashboard";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { isAxiosReady } from './services/axiosConfig';

function App() {

  const [axiosInitialized, setAxiosInitialized] = useState(false);

  useEffect(() => {
    isAxiosReady.then(() => {
      setAxiosInitialized(true);
    });
  }, []);

  if (!axiosInitialized) {
    return <div>Loading...</div>; // You can add a better loading spinner or animation here
  }
  return (
    <AuthState>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <Router>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/*" element={<Dashboard />} />
        </Routes>
      </Router>
    </AuthState>
  );
}

export default App;
