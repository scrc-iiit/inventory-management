import React, { useContext, useEffect, useState } from "react";
import { Box, CssBaseline } from "@mui/material";
import { Routes, Route, useNavigate } from "react-router-dom";
import Appbar from "../components/Appbar";
import Sidebar from "../components/Sidebar";
import AHome from "../components/AHome";
import UHome from "../components/UHome";
import Purchasedlog from "../components/Purchasedlog";
import Addstocks from "../components/Addstocks";
import Stockslog from "../components/Stockslog";
import Buystocks from "../components/Buystocks";
import Notfound from "../components/Notfound";
import AuthContext from "../context/auth/AuthContext";
import Itemstate from "../context/Items/Itemstate";
import UserManagement from "../components/UserManagement";
import Signup from "../components/Signup";
import Defectivelist from "../components/Defectivelist";
import UChangePassword from "../components/UChangePassword";
import { StackedBarChart } from "@mui/icons-material";
import DefectiveItemsChart from "../components/defective";
import PieChart from "../components/piechart";




const userRoutes = [
  { path: "/home", element: <UHome /> },
  { path: "/requestlog", element: <Purchasedlog /> },
  { path: "/store", element: <Buystocks /> },
  { path: "*", element: <UHome /> },
];

const adminRoutes = [
  { path: "/home", element: <AHome /> },
  { path: "/assign", element: <Signup /> },
  { path: "/acquire", element: <Addstocks /> },
  { path: "/stocklog", element: <Stockslog /> },
  { path: "/requestlog", element: <Purchasedlog /> },
  { path: "/store", element: <Buystocks /> },
  { path: "/defective", element: <Defectivelist /> },
  { path: "*", element: <AHome /> },
  { path: "/users", element: <UserManagement /> },
  { path: "/stackedbar", element: <StackedBarChart /> },
  { path: "/defective", element: <DefectiveItemsChart  /> },
  { path: "/piechart", element: <PieChart  /> },

  
  
];

const RoleBasedRoutes = ({ role }) => {
  const routes = role === "user" ? userRoutes : adminRoutes;
  return (
    <Routes>
      {routes.map((route, index) => (
        <Route key={index} path={route.path} element={route.element} />
      ))}
      <Route path="/forgot" element={<UChangePassword />} />
    </Routes>
  );
};

function Dashboard() {
  const context = useContext(AuthContext);
  const { getUser } = context;
  const navigate = useNavigate();
  const [role, setRole] = useState();

  useEffect(() => {
    async function fetchUserData() {
      if (localStorage.getItem("token")) {
        let response = await getUser();
        setRole(response.role);
      } else {
        navigate("/login");
      }
    }
    fetchUserData();
    // eslint-disable-next-line
  }, []);
  // routesConfig.js

  return (
    <Itemstate>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        {/* Appbar */}
        <Appbar />
        {/* Sidebar */}
        <Sidebar role={role} />
        {/* Body container */}

        <RoleBasedRoutes role={role} />
      </Box>
    </Itemstate>
  );
}

export default Dashboard;
