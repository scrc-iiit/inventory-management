import React, { useState, useEffect, useContext } from "react";
import { styled } from "@mui/material/styles";
import { Paper, Box, Grid, AppBar, Toolbar, IconButton, Typography } from "@mui/material";
import DownloadIcon from "@mui/icons-material/Download";
import ClearIcon from "@mui/icons-material/Clear";
import { jsPDF } from "jspdf";
import StackedBarChartWithData from "./stackedbar";
import DefectiveItemsChart from "./defective";
import PieChart from "./piechart";

// Import ItemContext to access notifications and alarms
import Itemcontext  from "../context/Items/Itemcontext"; // Correct the import path

// Styled components
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: "#F3F3F3",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
  height: "45vh",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  position: "relative",
}));

const Header = styled("div")(({ theme }) => ({
  position: "absolute",
  top: theme.spacing(1),
  left: theme.spacing(1),
  fontSize: "1rem",
  color: "#123462",
}));

const LogBoxContainer = styled(Paper)(({ theme }) => ({
  backgroundColor: "#FFFFFF",
  padding: theme.spacing(2),
  marginTop: theme.spacing(3),
  height: "35vh",
  border: "1px solid black",
  overflowY: "auto",
  position: "relative",
  width: "100%",
}));

const containerStyle = {
  width: "100%",
  height: "80vh",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

// Main AHome Component
const AHome = () => {
  const { notifications, alarms } = useContext(Itemcontext); // Use context to access notifications and alarms
  const [logs, setLogs] = useState([]);

  useEffect(() => {
    // Handle empty notifications or alarms gracefully
    const notificationLogs = notifications && notifications.length > 0
      ? notifications.map((notification) => ({
          message: notification.purpose || "Notification received",
          timestamp: new Date(notification.createdAt).toLocaleString(),
        }))
      : [];

    const alarmLogs = alarms && alarms.length > 0
      ? alarms.map((alarm) => ({
          message: alarm.message || "Alarm triggered",
          timestamp: new Date(alarm.timestamp).toLocaleString(),
        }))
      : [];

    // Combine and sort logs by timestamp (newest first)
    const allLogs = [...notificationLogs, ...alarmLogs].sort(
      (a, b) => new Date(b.timestamp) - new Date(a.timestamp)
    );

    setLogs(allLogs); // Update logs state
  }, [notifications, alarms]); // Re-run the effect if notifications or alarms change

  const handleDownload = () => {
    const doc = new jsPDF();
    const logContent = logs.map((log) => `${log.timestamp}: ${log.message}`).join("\n");
    doc.text(logContent, 10, 10);
    doc.save("log_data.pdf");
  };

  const clearLogs = () => {
    setLogs([]); // Clear all logs
  };

  return (
    <Box sx={containerStyle}>
      <Grid container spacing={1} alignItems="stretch">
        <Grid item xs={12} sm={4} md={4} sx={{ height: "40vh" }}>
          <Item sx={{ height: "50vh", bottom: "2vw" }}>
            <Header>Stocks Over Time</Header>
            <StackedBarChartWithData stockData={{ series: [], categories: [] }} />
          </Item>
        </Grid>
        <Grid item xs={12} sm={4} md={4}>
          <Item sx={{ height: "50vh", bottom: "2vw" }}>
            <Header>Component Usage</Header>
            <PieChart />
          </Item>
        </Grid>
        <Grid item xs={12} sm={4} md={4}>
          <Item sx={{ height: "50vh", bottom: "2vw" }}>
            <Header>Damaged Item Chart</Header>
            <DefectiveItemsChart />
          </Item>
        </Grid>
      </Grid>

      <LogBox logs={logs} clearLogs={clearLogs} handleDownload={handleDownload} />
    </Box>
  );
};

// Log Box Component
const LogBox = ({ logs, clearLogs, handleDownload }) => (
  <LogBoxContainer style={{ bottom: "2vw" }}>
    <AppBar position="sticky" style={{ backgroundColor: "#123462", boxShadow: "none", top: "-1.2vw" }}>
      <Toolbar>
        <Typography variant="h5" color="inherit">
          Log
        </Typography>
        <Box sx={{ flexGrow: 1 }} />
        <IconButton onClick={handleDownload} color="inherit">
          <DownloadIcon />
        </IconButton>
        <IconButton onClick={clearLogs} color="inherit">
          <ClearIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
    <Box sx={{ padding: 2, backgroundColor: "#fff" }}>
      {logs.length > 0 ? (
        logs.map((entry, index) => (
          <Typography variant="body2" key={index}>
            {entry.timestamp}: {entry.message}
          </Typography>
        ))
      ) : (
        <Typography variant="body2">No logs available</Typography>
      )}
    </Box>
  </LogBoxContainer>
);

export default AHome;
