import React, { useContext, useEffect, useState } from "react";
import {
  Box,
  Typography,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import Itemcontext from "../context/Items/Itemcontext";
import Addnewstocks from "./Addstocks/Addnewstocks";
import axios from "axios";

// Styles
const containerStyle = {
  width: "100%",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

const searchBarStyle = {
  height: "1vw",
};

// Status highlight function
function highlightStatus(stocks) {
  if (stocks > 5) return "🟢";
  if (stocks === 0) return "🔴";
  return "🟡";
}

const Addstocks = () => {
  const context = useContext(Itemcontext);
  const { StockList, stockList = [] } = context;

  const [search, setSearch] = useState("");
  const [openNew, setOpenNew] = useState(false);
  const [openUpdate, setOpenUpdate] = useState(false);
  const [currentRow, setCurrentRow] = useState(null);

  // Fetch stock data on component mount
  useEffect(() => {
    async function fetchStockData() {
      await StockList();
    }
    fetchStockData();
  }, [StockList]);

  // Filter data based on search input
  const filteredData = stockList?.filter((row) => {
    if (!row || typeof row.name === "undefined" || typeof row.SKU === "undefined") return false;
    if (search.includes(">") && parseInt(search.slice(1)) < row.stocks) return true;
    if (search.includes("<") && parseInt(search.slice(1)) > row.stocks) return true;
    if (search.includes(">=") && parseInt(search.slice(2)) <= row.stocks) return true;
    if (search.includes("<=") && parseInt(search.slice(2)) >= row.stocks) return true;
    return row.name.toLowerCase().includes(search.toLowerCase()) || row.SKU.toLowerCase().includes(search.toLowerCase());
  });

  // Handle dialog actions
  const handleOpenNew = () => setOpenNew(true);
  const handleCloseNew = () => setOpenNew(false);
  const handleOpenUpdate = (row) => {
    setCurrentRow(row);
    setOpenUpdate(true);
  };
  const handleCloseUpdate = () => setOpenUpdate(false);

  // Handle update dialog input changes
  const handleUpdateChange = (e) => {
    const { name, value } = e.target;
    setCurrentRow((prevRow) => ({ ...prevRow, [name]: value }));
  };

  // Handle update submission
  const handleUpdateSubmit = async () => {
    if (!currentRow || !currentRow._id) {
      console.error("Invalid data: Current row or its ID is missing.");
      return;
    }

    try {
      const apiUrl = process.env.REACT_APP_BACKEND_API_URL; // Get the base URL from the environment variable
      const endpoint = `${apiUrl}/api/stock/update/${currentRow._id}`; // Construct the full endpoint URL

      const response = await axios.put(endpoint, currentRow); // Send the PUT request to update the stock

      console.log("Stock updated successfully:", response.data);

      // Refresh the stock list after a successful update
      await StockList();

      // Close the update dialog/modal
      setOpenUpdate(false);
    } catch (error) {
      console.error("Failed to update stock:", error);
    }
  };

  return (
    <Box sx={containerStyle}>
      {/* Header Section */}
      <Box display="flex" justifyContent="space-between" alignItems="center" mb={2}>
        <Typography variant="h4">All Components</Typography>
        <Box display="flex" alignItems="center">
          <TextField
            variant="outlined"
            placeholder="Search..."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            inputProps={{ style: searchBarStyle }}
            sx={{ mr: 2 }}
          />
          <Button variant="contained" color="primary" onClick={handleOpenNew}>
            Add New Component
          </Button>
        </Box>
      </Box>

      {/* Add New Component Dialog */}
      <Addnewstocks open={openNew} handleClose={handleCloseNew} />

      {/* Update Component Dialog */}
      <Dialog open={openUpdate} onClose={handleCloseUpdate}>
        <DialogTitle>Update Component</DialogTitle>
        <DialogContent>
          <TextField
            fullWidth
            margin="dense"
            label="Name"
            name="name"
            value={currentRow?.name || ""}
            onChange={handleUpdateChange}
          />
          <TextField
            fullWidth
            margin="dense"
            label="SKU"
            name="SKU"
            value={currentRow?.SKU || ""}
            onChange={handleUpdateChange}
          />
          <TextField
            fullWidth
            margin="dense"
            label="Location"
            name="location"
            value={currentRow?.location || ""}
            onChange={handleUpdateChange}
          />
          <TextField
            fullWidth
            margin="dense"
            label="Stocks"
            name="stocks"
            type="number"
            value={currentRow?.stocks || ""}
            onChange={handleUpdateChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseUpdate} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleUpdateSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>

      {/* Stock Table */}
      <TableContainer component={Paper} sx={{ maxHeight: "34.6vw" }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Name</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>SKU</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Location</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Stocks</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody sx={{ bgcolor: "#B4B4B8" }}>
            {filteredData.map((row, index) => (
              <TableRow key={index} sx={{ "&:hover": { bgcolor: "#C7C8CC" } }}>
                <TableCell>{highlightStatus(row.stocks)}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.SKU}</TableCell>
                <TableCell>{row.location}</TableCell>
                <TableCell>{row.stocks}</TableCell>
                <TableCell>
                  <Button variant="contained" color="secondary" onClick={() => handleOpenUpdate(row)}>
                    Edit
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default Addstocks;
