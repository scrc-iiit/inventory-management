import React, { useContext, useState } from "react";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { Autocomplete, Box, Button, Grid, Modal, TextField, Typography } from "@mui/material";
import Itemcontext from "../../context/Items/Itemcontext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function AddBuyStocks({ open, handleClose, data }) {
  const context = useContext(Itemcontext);
  const { AddBuyStock } = context;
  const [sku, setSku] = useState("");
  const [quantity, setQuantity] = useState("");
  const [purchaseDate, setPurchaseDate] = useState(null);
  const [storeName, setStoreName] = useState("");
  const [price, setPrice] = useState("");
  const [purpose, setPurpose] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (sku && quantity > 0 && purchaseDate && storeName && price > 0) {
      try {
        let res = await AddBuyStock(sku, quantity, purchaseDate.format("YYYY-MM-DD"), storeName, price, purpose);
        if (res && res.msg) {
          console.log(res.msg);
          toast.success("Stock added successfully!");
        } else {
          toast.error("Something went wrong.");
        }
      } catch (error) {
        console.error("Error adding stock:", error);
        toast.error("Error adding stock.");
      }
    } else {
      toast.warn("Please fill all fields correctly.", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }

    // Clear form fields after submission
    setSku("");
    setQuantity("");
    setPurchaseDate(null);
    setStoreName("");
    setPrice("");
    setPurpose("");

    // Close the modal
    handleClose();
  };

  return (
    <Modal open={open} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Add Purchased Component
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Autocomplete
                options={data}
                getOptionLabel={(option) => `${option.SKU} - ${option.name}`}
                onChange={(event, newValue) => {
                  if (newValue) setSku(newValue.SKU);
                }}
                renderInput={(params) => <TextField {...params} label="SKU" variant="outlined" fullWidth />}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label="Quantity"
                variant="outlined"
                type="number"
                value={quantity}
                onChange={(e) => setQuantity(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                  label="Purchase Date"
                  value={purchaseDate}
                  onChange={(newValue) => setPurchaseDate(newValue)}
                  renderInput={(params) => <TextField {...params} fullWidth />}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label="Store Name"
                variant="outlined"
                value={storeName}
                onChange={(e) => setStoreName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label="Price"
                variant="outlined"
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label="Purpose"
                variant="outlined"
                value={purpose}
                onChange={(e) => setPurpose(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Submit
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

export default AddBuyStocks;
