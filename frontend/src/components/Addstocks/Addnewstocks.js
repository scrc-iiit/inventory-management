import { Box, Button, Grid, Modal, TextField, Typography } from "@mui/material";
import React, { useContext, useState } from "react";
import Itemcontext from "../../context/Items/Itemcontext";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function Addnewstocks({ open, handleClose }) {
  const { AddNewStock } = useContext(Itemcontext);
  const [formData, setFormData] = useState({ name: "", location: "" });
  const [error, setError] = useState("");

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const { name, location } = formData;

    if (!name || !location) {
      setError("All fields are required.");
      return;
    }

    try {
      const res = await AddNewStock(name, location);
      console.log("Form submitted with:", res);

      // Clear form fields
      setFormData({ name: "", location: "" });
      setError("");

      // Close the modal
      handleClose();
    } catch (error) {
      console.error("Failed to add new stock:", error);
      setError("Failed to add new stock. Please try again.");
    }
  };

  return (
    <Modal open={open} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Add New Component
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label="Name"
                name="name"
                variant="outlined"
                value={formData.name}
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                label="Location"
                name="location"
                variant="outlined"
                value={formData.location}
                onChange={handleInputChange}
              />
            </Grid>
            {error && (
              <Grid item xs={12}>
                <Typography color="error">{error}</Typography>
              </Grid>
            )}
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Submit
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

export default Addnewstocks;
