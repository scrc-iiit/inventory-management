import React, { useContext, useEffect, useState } from "react";
import logo1 from "../IIITH-logo.png";
import logo2 from "../scrc-logo.png";
import {
  Box,
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Badge,
  Popover,
  Card,
  CardContent,
  Button,
  Tooltip,
} from "@mui/material";
import {
  LogoutTwoTone,
  Notifications as NotificationsIcon,
  Alarm as AlarmIcon,
  Key as KeyIcon,
  Person2Sharp as Person2SharpIcon,
} from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import AuthContext from "../context/auth/AuthContext";
import Itemcontext from "../context/Items/Itemcontext";

function Appbar() {
  const { logout, getUser } = useContext(AuthContext);
  const {
    fetchNotificationCount,
    fetchNotifications,
    fetchAlarmCount,
    fetchAlarms,
    markNotificationAsRead,
    markAlarmAsResponded,
    notificationCount,
    notifications,
    alarmCount,
    alarms,
  } = useContext(Itemcontext);

  const [name, setName] = useState("");
  const [role, setRole] = useState("");
  const [notificationAnchorEl, setNotificationAnchorEl] = useState(null);
  const [alarmAnchorEl, setAlarmAnchorEl] = useState(null);

  const navigate = useNavigate();

  // Fetch user data on component mount
  useEffect(() => {
    async function fetchUserData() {
      const response = await getUser();
      setName(response.username);
      setRole(response.role);
    }
    fetchUserData();
  }, [getUser]);

  // Fetch data based on role and set interval for auto-fetching
  useEffect(() => {
    if (role === "admin" || role === "sysadmin") {
      const fetchAllData = async () => {
        await Promise.all([
          fetchNotificationCount(),
          fetchNotifications(),
          fetchAlarmCount(),
          fetchAlarms(),
        ]);
      };

      fetchAllData(); // Initial fetch

      const intervalId = setInterval(fetchAllData, 3 * 60 * 1000); // 3-minute interval

      return () => clearInterval(intervalId); // Cleanup on unmount
    }
  }, [role, fetchNotificationCount, fetchNotifications, fetchAlarmCount, fetchAlarms]);

  const handleLogout = () => {
    logout();
    navigate("/login");
  };

  const handleNotificationClick = (event) => {
    setNotificationAnchorEl(event.currentTarget);
  };

  const handleNotificationClose = () => {
    setNotificationAnchorEl(null);
  };

  const handleAlarmClick = (event) => {
    setAlarmAnchorEl(event.currentTarget);
  };

  const handleAlarmClose = () => {
    setAlarmAnchorEl(null);
  };

  return (
    <div>
      <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1, bgcolor: "#123462" }}>
        <Toolbar>
          <Box component="img" sx={{ height: 50, px: 1 }} alt="IIITH Logo" src={logo1} />
          <Box component="img" sx={{ height: 50, px: 1 }} alt="SCRC Logo" src={logo2} />
          <Box sx={{ flexGrow: 1, display: "flex", justifyContent: "center" }}>
            <Typography variant="h6" component="h1" sx={{ color: "#F3F3F3" }}>
              Inventory Management Dashboard
            </Typography>
          </Box>

          {(role === "admin" || role === "sysadmin") && (
            <>
              <IconButton onClick={handleNotificationClick} sx={{ ml: 2 }}>
                <Badge badgeContent={notificationCount} color="warning" max={999}>
                  <NotificationsIcon sx={{ color: "#F3F3F3" }} />
                </Badge>
              </IconButton>
              <IconButton onClick={handleAlarmClick} sx={{ ml: 2 }}>
                <Badge badgeContent={alarmCount} color="error" max={999}>
                  <AlarmIcon sx={{ color: "#F3F3F3" }} />
                </Badge>
              </IconButton>
            </>
          )}
          <Tooltip
            title={
              <Typography variant="h6" sx={{ fontSize: "1rem" }}>
                {name}
              </Typography>
            }
            arrow
          >
            <Person2SharpIcon sx={{ ml: 1.5 }} />
          </Tooltip>
          <IconButton color="inherit" onClick={() => navigate("/forgot")}>
            <KeyIcon sx={{ color: "#F3F3F3" }} />
          </IconButton>
          <IconButton color="inherit" onClick={handleLogout}>
            <LogoutTwoTone sx={{ color: "#F3F3F3" }} />
          </IconButton>
        </Toolbar>
      </AppBar>

      {/* Notifications Popover */}
      <Popover
        open={Boolean(notificationAnchorEl)}
        anchorEl={notificationAnchorEl}
        onClose={handleNotificationClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <Box padding={2} width="25vw">
          <Typography variant="h6">Notifications</Typography>
          {notifications.length > 0 ? (
            notifications
              .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
              .map((notification) => (
                <Card key={notification._id} sx={{ marginBottom: 2 }}>
                  <CardContent>
                    <Typography variant="body1">{`SKU: ${notification.parameter}`}</Typography>
                    <Typography variant="body2">{`Purpose: ${notification.purpose}`}</Typography>
                    <Typography variant="body2">{`Time: ${new Date(notification.createdAt).toLocaleString()}`}</Typography>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => markNotificationAsRead(notification._id)}
                      sx={{ marginTop: 1 }}
                    >
                      Mark as Read
                    </Button>
                  </CardContent>
                </Card>
              ))
          ) : (
            <Typography>No notifications found.</Typography>
          )}
        </Box>
      </Popover>

      {/* Alarms Popover */}
      <Popover
        open={Boolean(alarmAnchorEl)}
        anchorEl={alarmAnchorEl}
        onClose={handleAlarmClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <Box padding={2} width="28vw">
          <Typography variant="h6">Alarms</Typography>
          {alarms.length > 0 ? (
            alarms
              .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
              .map((alarm) => (
                <Card key={alarm._id} sx={{ marginBottom: 2 }}>
                  <CardContent>
                    <Typography variant="body1">{`SKU: ${alarm.parameter}`}</Typography>
                    <Typography variant="body2">{`Purpose: ${alarm.purpose}`}</Typography>
                    <Typography variant="body2">{`Time: ${new Date(alarm.createdAt).toLocaleString()}`}</Typography>
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => markAlarmAsResponded(alarm._id)}
                      sx={{ marginTop: 1 }}
                    >
                      Mark as Responded
                    </Button>
                  </CardContent>
                </Card>
              ))
          ) : (
            <Typography>No alarms found.</Typography>
          )}
        </Box>
      </Popover>
    </div>
  );
}

export default Appbar;
