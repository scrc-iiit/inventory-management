import React, { useContext, useEffect, useState } from "react";
import { Box, Typography, TextField, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Button } from "@mui/material";
import Itemcontext from "../context/Items/Itemcontext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const containerStyle = {
  width: "100%",
  height: "50vh",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

const searchBarStyle = {
  height: "1vw",
};

function Buystocks() {
  const context = useContext(Itemcontext);
  const { StockList, stockList, BuyStock } = context;

  const [search, setSearch] = useState("");
  const [quantity, setQuantity] = useState({});
  const [remarks, setRemarks] = useState({});

  const data = stockList.filter((row) => {
    return row.stocks > 0;
  });

  useEffect(() => {
    async function fetchStockData() {
      await StockList();
    }
    fetchStockData();
    // eslint-disable-next-line
  }, []);

  // Filter the data based on the search input
  const filteredData = data.filter((row) => {
    return row.name.toLowerCase().includes(search.toLowerCase()) || row.SKU.toLowerCase().includes(search.toLowerCase());
  });

  const handleQuantityChange = (sku, value) => {
    setQuantity((prev) => ({ ...prev, [sku]: value }));
  };

  const handleRemarksChange = (sku, value) => {
    setRemarks((prev) => ({ ...prev, [sku]: value }));
  };

  const handleSubmit = async (sku, stock) => {
    console.log("Submit:", {
      sku,
      quantity: quantity[sku],
      remarks: remarks[sku],
    });
    // Add your submit logic here
    if (stock >= quantity[sku] && quantity[sku] > 0 && remarks[sku] && remarks[sku] !== "") {
      let res = await BuyStock(sku, remarks[sku], quantity[sku]);
      console.log(res);
      setRemarks((prev) => ({ ...prev, [sku]: "" }));
      setQuantity((prev) => ({ ...prev, [sku]: "" }));
      // window.location.reload();
    } else {
      toast.warn("Please carefully fill out all fields and submit", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  return (
    <Box sx={containerStyle}>
      <Box display="flex" justifyContent="space-between" alignItems="center" mb={2}>
        <Typography variant="h4">Available Components</Typography>
        <TextField
          variant="outlined"
          placeholder="Search..."
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          inputProps={{ style: searchBarStyle }}
        />
      </Box>
      <TableContainer component={Paper} sx={{ maxHeight: "34.6vw" }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Name</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Location</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Stocks</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Quantity Needed</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Purpose</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell>
            </TableRow>
          </TableHead>
          <TableBody sx={{ bgcolor: "#B4B4B8" }}>
            {filteredData.map((row, index) => (
              <TableRow key={index} sx={{ "&:hover": { bgcolor: "#C7C8CC" } }}>
                <TableCell>{row.stocks <= 5 ? "🔴" : "🟢"}</TableCell>
                <TableCell>
                  {row.SKU}
                  <br />
                  {row.name}
                </TableCell>
                <TableCell>{row.location}</TableCell>
                <TableCell>{row.stocks}</TableCell>
                <TableCell>
                  <TextField
                    type="number"
                    variant="outlined"
                    size="small"
                    value={quantity[row.SKU] || ""}
                    inputProps={{ min: 0, max: row.stocks }}
                    onChange={(e) => handleQuantityChange(row.SKU, e.target.value)}
                  />
                </TableCell>
                <TableCell>
                  <TextField
                    multiline
                    variant="outlined"
                    size="small"
                    rows={3}
                    value={remarks[row.SKU] || ""}
                    onChange={(e) => handleRemarksChange(row.SKU, e.target.value)}
                  />
                </TableCell>
                <TableCell>
                  <Button variant="contained" color="primary" onClick={() => handleSubmit(row.SKU, row.stocks)}>
                    Submit
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}

export default Buystocks;
