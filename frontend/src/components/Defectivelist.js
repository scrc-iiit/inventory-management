import React, { useContext, useEffect, useState } from "react";
import {
  Box,
  Typography,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Modal,
  Button,
  Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import Itemcontext from "../context/Items/Itemcontext";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import AuthContext from "../context/auth/AuthContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const containerStyle = {
  width: "100%",
  height: "50vh",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

const searchBarStyle = {
  height: "1vw",
};

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function DeleteRequest(props) {
  const context = useContext(Itemcontext);
  const { DefectiveDel } = context;

  const handleSubmit = async (event) => {
    event.preventDefault();
    // Handle form submission logic here
    let res = await DefectiveDel(props.row._id);
    console.log(res);

    // Close the modal
    props.handleClose();
  };
  return (
    <Modal open={props.open} onClose={props.handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Delete Permanently
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Delete
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

function AddRequest(props) {
  const context = useContext(Itemcontext);
  const { AddDefective, Defectiveitems } = context;

  const vlist = props.usersname;

  const [name, setName] = useState("");
  const [quantity, setQuantity] = useState("");
  const [cause, setCause] = useState("");
  const [verified, setVerified] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    // Handle form submission logic here
    if (name === "" || quantity === "" || cause === "" || verified === "" || !verified) {
      toast.warn("Please carefully fill out all fields and submit", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      let res = await AddDefective(name, quantity, cause, verified);
      console.log(res);
    }
    setName("");
    setCause("");
    setQuantity("");
    setVerified("");

    await Defectiveitems();
    // Close the modal
    props.handleClose();
  };
  return (
    <Modal open={props.open} onClose={props.handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Add Defective item
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField fullWidth label="name" variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
            </Grid>
            <Grid item xs={12}>
              <TextField fullWidth label="Quantity" variant="outlined" type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
            </Grid>

            <Grid item xs={12}>
              <TextField fullWidth label="Cause" variant="outlined" value={cause} multiline rows={4} onChange={(e) => setCause(e.target.value)} />
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth margin="normal">
                <InputLabel id="role-label">Verified</InputLabel>
                <Select labelId="role-label" id="Verified" value={verified} label="Verified" onChange={(e) => setVerified(e.target.value)}>
                  {vlist.map((value, index) => (
                    <MenuItem key={index} value={value.username}>
                      {value.username}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Submit
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

const Defectivelist = () => {
  // Sample data for the table

  const context = useContext(Itemcontext);
  const { Defectiveitems, defectivedata } = context;

  const acontext = useContext(AuthContext);
  const { UserManage, usersdata } = acontext;

  useEffect(() => {
    async function fetchStockData() {
      await Defectiveitems();
      await UserManage();
    }
    fetchStockData();
    // eslint-disable-next-line
  }, []);

  const [search, setSearch] = useState("");
  const [selectedRow, setSelectedRow] = useState({});

  const [opendel, setOpendel] = useState(false);
  const handleClosedel = () => setOpendel(false);
  const handleOpendel = (row) => {
    setSelectedRow(row);
    setOpendel(true);
  };

  const [openadd, setOpenadd] = useState(false);
  const handleCloseadd = () => setOpenadd(false);
  const handleOpenadd = () => {
    setOpenadd(true);
  };

  // Filter the data based on the search input
  const filteredAndSortedData = defectivedata
    .filter((row) => row.name.toLowerCase().includes(search.toLowerCase()) || row.date.toLowerCase().includes(search.toLowerCase()))
    .sort((a, b) => new Date(b.date) - new Date(a.date));

  //  console.log(defectivedata)
  return (
    <Box sx={containerStyle}>
      <Box display="flex" justifyContent="space-between" alignItems="center" mb={2}>
        <Typography variant="h4">Defective items</Typography>
        <Box display="flex" alignItems="center">
          <TextField
            variant="outlined"
            placeholder="Search..."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            inputProps={{ style: searchBarStyle }}
            sx={{ mr: 2 }}
          />
          <Button variant="contained" color="primary" sx={{ mr: 2 }} onClick={() => handleOpenadd()}>
            Add
          </Button>
        </Box>
      </Box>
      <DeleteRequest open={opendel} handleClose={handleClosedel} row={selectedRow} />
      <AddRequest open={openadd} handleClose={handleCloseadd} usersname={usersdata} />
      <TableContainer component={Paper} sx={{ maxHeight: "35vw" }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Name</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Quantity</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Cause</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Verified</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>date</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell>
            </TableRow>
          </TableHead>
          <TableBody sx={{ bgcolor: "#B4B4B8" }}>
            {filteredAndSortedData.map((row, index) => (
              <TableRow key={index} sx={{ "&:hover": { bgcolor: "#C7C8CC" } }}>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.quantity}</TableCell>
                <TableCell>{row.cause}</TableCell>
                <TableCell>{row.verify}</TableCell>
                <TableCell>{row.date}</TableCell>
                <TableCell>
                  <DeleteTwoToneIcon style={{ color: "#860A35" }} onClick={() => handleOpendel(row)} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default Defectivelist;
