import React, { useContext, useState } from "react";
import logo1 from "../scrc-logo.png";
import logo2 from "../IIITH-logo.png";
import { TextField, Button, Box, Container, Typography, IconButton, InputAdornment } from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import AuthContext from "../context/auth/AuthContext";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "50%",
  backgroundColor: "#F3F3F3",
  color: "#123462",
  p: 4,
};

function LoginPage() {
  const context = useContext(AuthContext);
  const { login } = context;
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = (event) => event.preventDefault();
  const [auth, setAuth] = useState({ username: "", password: "" });
  const onchange = (e) => {
    setAuth({ ...auth, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await login(auth.username, auth.password);
      if (response.status !== true) {
        console.log(response.msg);
      } else {
        navigate("/Ahome");
      }
    } catch (err) {
      // alert("err");
      toast.warn("Incorrect or Invalid Credentials", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });

    }
  };
  return (
    <div style={style}>
      <Container component="main" maxWidth="xs" sx={{ pb: 10 }}>
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Box sx={{ flexDirection: "row", py: 2 }}>
            <Box component="img" sx={{ height: 80, px: 2 }} alt="Logo" src={logo2} />
            <Box component="img" sx={{ height: 80, px: 2 }} alt="Logo" src={logo1} />
          </Box>
          <Typography component="h1" variant="h5">
            IMS Dashboard
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              onChange={onchange}
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type={showPassword ? "text" : "password"}
              id="password"
              autoComplete="current-password"
              onChange={onchange}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }} onClick={handleSubmit}>
              Sign In
            </Button>
          </Box>
        </Box>
      </Container>
    </div>
  );
}

export default LoginPage;
