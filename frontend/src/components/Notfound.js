import { useNavigate } from "react-router-dom";
import React from "react";

const style = {
  position: "absolute",
  top: "50vh",
  left: "100vh",
  transform: "translate(-50%, -50%)",
  width: "100vh",
  height: "80vh",
  color: "#123462",
  padding: 8,
  backgroundImage: `url("https://cdn.dribbble.com/users/62549/screenshots/3894903/media/f2c7c60b8dc9870264cdcb1ef2a6cb5f.gif")`, // Use backticks for string interpolation
  backgroundSize: "cover", // Optional: adjust background size if needed
  backgroundPosition: "center", // Optional: adjust background position if needed
};

function Notfound() {
  let Navigate = useNavigate();
  return (
    <div
      style={style}
      onClick={() => {
        Navigate("/");
      }}
    ></div>
  );
}

export default Notfound;
