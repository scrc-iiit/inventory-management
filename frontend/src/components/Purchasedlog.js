import React, { useContext, useEffect, useState } from "react";
import {
  Box,
  Typography,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Modal,
  Button,
  Grid,
} from "@mui/material";
import ThumbUpAltTwoToneIcon from "@mui/icons-material/ThumbUpAltTwoTone";
import ThumbDownAltTwoToneIcon from "@mui/icons-material/ThumbDownAltTwoTone";
import PendingActionsTwoToneIcon from "@mui/icons-material/PendingActionsTwoTone";
import Itemcontext from "../context/Items/Itemcontext";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import SwapCallsTwoToneIcon from "@mui/icons-material/SwapCallsTwoTone";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const containerStyle = {
  width: "100%",
  height: "50vh",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

const searchBarStyle = {
  height: "1vw",
};

const statusIcons = {
  Approved: <ThumbUpAltTwoToneIcon style={{ color: "#508D69" }} />,
  Pending: <PendingActionsTwoToneIcon style={{ color: "#EEF296" }} />,
  Rejected: <ThumbDownAltTwoToneIcon style={{ color: "#860A35" }} />,
};

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function DeleteRequest(props) {
  const context = useContext(Itemcontext);
  const { RequestDel } = context;

  const handleSubmit = async (event) => {
    event.preventDefault();
    // Handle form submission logic here
    let res = await RequestDel(props.row._id);
    console.log(res);

    // Close the modal
    props.handleClose();
  };
  return (
    <Modal open={props.open} onClose={props.handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Delete Permanently
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Delete
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

function ReturnItem(props) {
  const context = useContext(Itemcontext);
  const { BuyStock } = context;

  const [quantity, setQuantity] = useState("");

  const handleonchange = (e) => {
    setQuantity(e.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
   
    // Handle form submission0 < quantity, "&&", logic here
    console.log( quantity < props.row.usedQuantity, "check",  quantity ,"and",  props.row.usedQuantity);
    if (0 < Number(quantity)&& Number(quantity) && Number(quantity) < Number(props.row.usedQuantity)) {
      await BuyStock(props.row.SKU, "Return request for extra item", -quantity);
    } else {
      // console.log(res);
      toast.warn("Please carefully fill out return quantity and submit", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    setQuantity("");
    // Close the modal
    props.handleClose();
  };
  return (
    <Modal open={props.open} onClose={props.handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Return Item
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField type="number" variant="outlined" size="small" placeholder="Return Quantity" value={quantity} onChange={handleonchange} />
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Return
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

const UpdateRequest = (props) => {
  const context = useContext(Itemcontext);
  const { ReqUpdate } = context;

  const [isRejected, setIsRejected] = useState(false);
  const [remarks, setRemarks] = useState('');

  // Handle form submission for approval or rejection
  const handleSubmit = async (event) => {
    event.preventDefault();
    const status = event.target.value;

    if (status === 'Rejected') {
      setIsRejected(true); // Show remarks input for rejection
    } else if (status === 'Approved') {
      try {
        await ReqUpdate(props.row._id, status); // Send approved status
        props.handleClose(); // Close the modal after approval
      } catch (error) {
        console.error('Error submitting approval:', error);
      }
    }
  };

  

  // Handle the final submission of rejection along with remarks
  const handleRemarksSubmit = async () => {
    if (remarks.trim()) {
      try {
        await ReqUpdate(props.row._id, 'Rejected', remarks); // Send rejected status with remarks
        setIsRejected(false);
        setRemarks(''); // Clear remarks after submission
        props.handleClose(); // Close the modal
      } catch (error) {
        console.error('Error submitting rejection:', error);
      }
    } else {
      alert('Please provide remarks before submitting.');
    }
  };

  let query = props.row.status === 'Approved' || props.row.status === 'Rejected';

  return (
    <Modal
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        {!query ? (
          <>
            <Typography id="modal-modal-title" variant="h6" gutterBottom>
              Update Request
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  value="Approved"
                  variant="contained"
                  color="primary"
                  onClick={handleSubmit}
                >
                  Approved
                </Button>
              </Grid>
              <Grid item xs={12}>
                <Button
                  type="submit"
                  value="Rejected"
                  variant="contained"
                  color="primary"
                  onClick={handleSubmit}
                >
                  Rejected
                </Button>
              </Grid>

              {isRejected && (
                <>
                  <Grid item xs={12}>
                    <TextField
                      fullWidth
                      label="Remarks"
                      variant="outlined"
                      value={remarks}
                      onChange={(e) => setRemarks(e.target.value)}
                      multiline
                      rows={4}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={handleRemarksSubmit}
                    >
                      Submit Rejection
                    </Button>
                  </Grid>
                </>
              )}
            </Grid>
          </>
        ) : (
          <Typography id="modal-modal-title" variant="h6" gutterBottom>
            Update can't be changed
          </Typography>
        )}
      </Box>
    </Modal>
  );
};

const Purchasedlog = () => {
  // Sample data for the table

  const context = useContext(Itemcontext);
  const { RequestList, requestData, StockList, stockList } = context;

  useEffect(() => {
    async function fetchStockData() {
      await RequestList();
      await StockList(); // Fetch stock list for item names
    }
    fetchStockData();
    // eslint-disable-next-line
  }, []);

  const [search, setSearch] = useState("");
  const [selectedRow, setSelectedRow] = useState({});
   const [openEdit, setOpenEdit] = useState(false);

  const handleOpenEdit = (row) => {
    setSelectedRow(row); // Set selected row data to be passed into dialog
    setOpenEdit(true);
  };

  const handleCloseEdit = () => setOpenEdit(false);
  const [opendel, setOpendel] = useState(false);
  const handleClosedel = () => setOpendel(false);
 
  const handleOpendel = (row) => {
    setSelectedRow(row);
    setOpendel(true);
  };
  const [opench, setOpench] = useState(false);
  const handleClosech = () => setOpench(false);
  const handleOpench = (row) => {
    setSelectedRow(row);
    setOpench(true);
  };

  const [openReturn, setOpenReturn] = useState(false);
  const handleCloseReturn = () => setOpenReturn(false);
  const handleOpenReturn = (row) => {
    setSelectedRow(row);
    setOpenReturn(true);
  };

  // Merge requestData with stockList to add the `name` field based on SKU
  const mergedData = requestData.map((row) => {
    const stockItem = stockList.find((item) => item.SKU === row.SKU);
    return { ...row, name: stockItem ? stockItem.name : "N/A" };
  });


  // Filter the data based on the search input
  const filteredData = mergedData.filter(
    (row) =>
      row.SKU.toLowerCase().includes(search.toLowerCase()) ||
      row.status.toLowerCase().includes(search.toLowerCase()) ||
      row.userName.toLowerCase().includes(search.toLowerCase())
  );

  // Define keyframes for the surprise emoji animation
  const surpriseEmojiStyle = {
    animation: "surprise 1s ease-in-out infinite alternate",
  };

  // Add keyframes for the surprise emoji
  const keyframes = `
    @keyframes surprise {
      0% { transform: rotate(0deg); }
      50% { transform: rotate(15deg); }
      100% { transform: rotate(-15deg); }
    }
  `;

    // Inject keyframes into the document head
    useEffect(() => {
      const styleSheet = document.createElement("style");
      styleSheet.type = "text/css";
      styleSheet.innerText = keyframes;
      document.head.appendChild(styleSheet);
    }, []);

  return (
    <Box sx={containerStyle}>
      <Box display="flex" justifyContent="space-between" alignItems="center" mb={2}>
        <Typography variant="h4">Request Log</Typography>

        <TextField
          variant="outlined"
          placeholder="Search..."
          value={search}
          onChange={(e) => setSearch(e.target.value)}
          inputProps={{ style: searchBarStyle }}
        />
      </Box>

      {/* <UpdateDialog
        open={openUpdate}
        handleClose={handleCloseUpdate}
        row={selectedRow}
        handleUpdate={handleUpdate}
      /> */}
       {/* <EditRequestDialog open={openEdit} handleClose={handleCloseEdit} row={selectedRow} /> */}
      <DeleteRequest open={opendel} handleClose={handleClosedel} row={selectedRow} />
      <UpdateRequest open={opench} handleClose={handleClosech} row={selectedRow} />
      <ReturnItem open={openReturn} handleClose={handleCloseReturn} row={selectedRow} />
      <TableContainer component={Paper} sx={{ maxHeight: "35vw" }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Name</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Username</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Stock Keeping Unit</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Quantity</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Purpose</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Date</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Status</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Remarks</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Responded</TableCell>
              {/* <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Action</TableCell> */}
              {localStorage.getItem("role") === "sysadmin" ? <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell> : ""}
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell>
            </TableRow>
          </TableHead>
          <TableBody sx={{ bgcolor: "#B4B4B8" }}>
            {filteredData.map((row, index) => (
              <TableRow key={index} sx={{ "&:hover": { bgcolor: "#C7C8CC" } }}>
                <TableCell>{row.usedQuantity < 0 ? <SwapCallsTwoToneIcon sx={{ color: "#01204E" }} /> : statusIcons[row.status]}</TableCell>
                <TableCell>{row.name}</TableCell>
                <TableCell>{row.userName}</TableCell>
                <TableCell>{row.SKU}</TableCell>
                <TableCell>{row.usedQuantity}</TableCell>
                <TableCell>{row.purpose}</TableCell>
                <TableCell>{row.date}</TableCell>
                <TableCell>
                  {localStorage.getItem("role") === "user" || row.status !== "Pending" ? (
                    row.status
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      sx={{ mr: 2 }}
                      onClick={() => {
                        handleOpench(row);
                      }}
                    >
                      Change
                    </Button>
                  )}
                </TableCell>
                <TableCell>{row.rejectionRemarks || 'N/A'}</TableCell> {/* Display remarks or N/A */}
                <TableCell>
                  {row.status === "Pending" ? (
                    <span role="img" aria-label="surprise" style={surpriseEmojiStyle}>
                      😲
                    </span>
                  ) : (
                    row.Reported_by || "N/A"
                  )}
                </TableCell>
                {/* <TableCell>
                  {row.status === "Pending" ? (
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => handleOpenEdit(row)}
                    >
                      Update
                    </Button>
                  ) : (
                    "N/A"
                  )}
                </TableCell> */}
                {localStorage.getItem("role") === "sysadmin" ? (
                  <TableCell>
                    <DeleteTwoToneIcon style={{ color: "#860A35" }} onClick={() => handleOpendel(row)} />
                  </TableCell>
                ) : (
                  ""
                )}
                <TableCell>
                  {row.status !== "Approved" ? (
                    <Typography sx={{ fontSize: "1.5rem" }}>❗</Typography>
                  ) : row.usedQuantity < 0 ? (
                    <Typography sx={{ fontSize: "1.5rem" }}>❗</Typography>
                  ) : (
                    <Button
                      variant="contained"
                      color="primary"
                      sx={{ mr: 2 }}
                      onClick={() => {
                        handleOpenReturn(row);
                      }}
                    >
                      Return
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default Purchasedlog;
