import React, { useState } from "react";
import { Box, Toolbar, List, ListItemIcon } from "@mui/material";
import { ListItem, ListItemButton, ListItemText } from "@mui/material";
import MuiDrawer from "@mui/material/Drawer";
import { styled } from "@mui/material/styles";
import HomeIcon from "@mui/icons-material/Home";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import DescriptionIcon from "@mui/icons-material/Description";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import BrokenImageIcon from "@mui/icons-material/BrokenImage";
import Groups2Icon from "@mui/icons-material/Groups2";
import ListIcon from "@mui/icons-material/List";
import { useNavigate } from "react-router-dom";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== "open" })(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

function Sidebar(props) {
  const [hidden, setHidden] = useState(true);

  return (
    <div>
      <Drawer
        variant="permanent"
        open={hidden}
        PaperProps={{
          sx: {
            backgroundColor: "#F3F3F3",
            color: "#123462",
          },
        }}
        onMouseEnter={() => setHidden(true)}
        onMouseLeave={() => setHidden(false)}
      >
        <Toolbar />
        <Box>{props.role === "user" ? <Usershow /> : <Adminshow />}</Box>
      </Drawer>
    </div>
  );
}

function Usershow() {
  const navigate = useNavigate();
  return (
    <List>
      <ListItem
        disablePadding
        onClick={() => {
          navigate("/home");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <HomeIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItemButton>
      </ListItem>

      <ListItem
        disablePadding
        onClick={() => {
          navigate("/store");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <AddShoppingCartIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Available Components" />
        </ListItemButton>
      </ListItem>

      <ListItem
        disablePadding
        onClick={() => {
          navigate("/requestlog");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <DescriptionIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Request Log" />
        </ListItemButton>
      </ListItem>
    </List>
  );
}

function Adminshow() {
  const navigate = useNavigate();
  return (
    <List>
      <ListItem
        disablePadding
        onClick={() => {
          navigate("/home");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <HomeIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItemButton>
      </ListItem>

      <ListItem
        disablePadding
        onClick={() => {
          navigate("/acquire");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <LocalMallIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Create SKU" />
        </ListItemButton>
      </ListItem>

      <ListItem
        disablePadding
        onClick={() => {
          navigate("/store");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <AddShoppingCartIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Available Components" />
        </ListItemButton>
      </ListItem>

      <ListItem
        disablePadding
        onClick={() => {
          navigate("/stocklog");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <ListIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Component Purchase" />
        </ListItemButton>
      </ListItem>

      <ListItem
        disablePadding
        onClick={() => {
          navigate("/requestlog");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <DescriptionIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Request Log" />
        </ListItemButton>
      </ListItem>

      <ListItem
        disablePadding
        onClick={() => {
          navigate("/defective");
        }}
      >
        <ListItemButton>
          <ListItemIcon>
            <BrokenImageIcon sx={{ color: "#123462" }} />
          </ListItemIcon>
          <ListItemText primary="Defective Items" />
        </ListItemButton>
      </ListItem>

      {localStorage.getItem("role") === "sysadmin" ? (
        <ListItem
          disablePadding
          onClick={() => {
            navigate("/users");
          }}
        >
          <ListItemButton>
            <ListItemIcon>
              <Groups2Icon sx={{ color: "#123462" }} />
            </ListItemIcon>
            <ListItemText primary="User Management" />
          </ListItemButton>
        </ListItem>
      ) : (
        ""
      )}
    </List>
  );
}

export default Sidebar;
