import React, { useContext, useEffect, useState } from "react";
import {
  Box,
  Typography,
  TextField,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Modal,
  Grid,
  Button,
} from "@mui/material";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import Itemcontext from "../context/Items/Itemcontext";
import AddBuyStocks from "./Addstocks/AddBuyStocks";

const containerStyle = {
  width: "100%",
  height: "50vh",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

const searchBarStyle = {
  height: "1vw",
};

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function DeleteStock(props) {
  const context = useContext(Itemcontext);
  const { StocklogDel } = context;

  const handleSubmit = async (event) => {
    event.preventDefault();
    // Handle form submission logic here
    let res = await StocklogDel(props.row._id);
    console.log(res);

    // Close the modal
    props.handleClose();
  };
  return (
    <Modal open={props.open} onClose={props.handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Delete Permanently
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Delete
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

const Stockslog = () => {
  const context = useContext(Itemcontext);
  const { Stocklog, datastocklog, StockList, stockList } = context;

  const [selectedRow, setSelectedRow] = useState({});
  const [opendel, setOpendel] = useState(false);
  const handleClosedel = () => setOpendel(false);
  const handleOpendel = (row) => {
    setSelectedRow(row);
    setOpendel(true);
  };

  const [openbuy, setOpenbuy] = React.useState(false);
  const handleOpenbuy = () => setOpenbuy(true);
  const handleClosebuy = () => setOpenbuy(false);

  const [search, setSearch] = useState("");

  useEffect(() => {
    async function fetchStockData() {
      await Stocklog();
      await StockList();
    }
    fetchStockData();
    // eslint-disable-next-line
  }, []);

  // Filter the data based on the search input
  const filteredData = datastocklog
    .filter((row) => {
      if (search.includes(">") && parseInt(search.slice(1)) < row.quantity.toString()) {
        return true;
      }
      if (search.includes("<") && parseInt(search.slice(1)) > row.quantity.toString()) {
        return true;
      }
      if (search.includes(">=") && parseInt(search.slice(2)) <= row.quantity.toString()) {
        return true;
      }
      if (search.includes("<=") && parseInt(search.slice(2)) >= row.quantity.toString()) {
        return true;
      }
      return (
        row.SKU.toLowerCase().includes(search.toLowerCase()) ||
        row.purchased_by.toLowerCase().includes(search.toLowerCase()) ||
        row.store_name.toLowerCase().includes(search.toLowerCase())
      );
    })
    .sort((a, b) => new Date(b.date) - new Date(a.date));
  return (
    <Box sx={containerStyle}>
      <Box display="flex" justifyContent="space-between" alignItems="center" mb={2}>
        <Typography variant="h4">Component Purchase logs</Typography>
        <Box display="flex" alignItems="center">
          <TextField
            variant="outlined"
            placeholder="Search..."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            inputProps={{ style: searchBarStyle }}
            sx={{ mr: 2 }}
          />
          <Button variant="contained" color="secondary" onClick={handleOpenbuy}>
            Add New Purchased
          </Button>
        </Box>
      </Box>
      <AddBuyStocks open={openbuy} handleClose={handleClosebuy} data={stockList} />
      <DeleteStock open={opendel} handleClose={handleClosedel} row={selectedRow} />
      <TableContainer component={Paper} sx={{ maxHeight: "35vw" }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Stock Keeping Unit</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Quantity</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Purchased By</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Price</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Store Name</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Purchased Date</TableCell>
              <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}>Purpose</TableCell>
              {localStorage.getItem("role") === "sysadmin" ? <TableCell sx={{ bgcolor: "#373A40", color: "#EEEEEE" }}></TableCell> : ""}
            </TableRow>
          </TableHead>
          <TableBody sx={{ bgcolor: "#B4B4B8" }}>
            {filteredData.map((row, index) => (
              <TableRow key={index} sx={{ "&:hover": { bgcolor: "#C7C8CC" } }}>
                <TableCell>{row.SKU}</TableCell>
                <TableCell>{row.quantity}</TableCell>
                <TableCell>{row.purchased_by}</TableCell>
                <TableCell>{row.price}</TableCell>
                <TableCell>{row.store_name}</TableCell>
                <TableCell>{row.purchase_date}</TableCell>
                <TableCell>{row.purpose}</TableCell>
                {localStorage.getItem("role") === "sysadmin" ? (
                  <TableCell>
                    <DeleteTwoToneIcon style={{ color: "#860A35" }} onClick={() => handleOpendel(row)} />
                  </TableCell>
                ) : (
                  ""
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default Stockslog;
