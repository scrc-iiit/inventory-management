import React, { useContext } from "react";
import { useState } from "react";
import { TextField, InputAdornment, IconButton, Button, Typography } from "@mui/material";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AuthContext from "../context/auth/AuthContext";

const style = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "80%",
  maxWidth: "400px",
  padding: "16px",
  border: "1px solid #000000",
  color: "#123462",
  boxSizing: "border-box",
};

function UChangePassword() {
  const context = useContext(AuthContext);
  const { ChangePassword } = context;
  const [showPassword, setShowPassword] = useState(false);
  const [confirmShowPassword, setConfirmShowPassword] = useState(false);
  const [newPassword, setNewPassword] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleClickShowConfirmPassword = () => setConfirmShowPassword(!confirmShowPassword);

  const handleSubmit = async () => {
    if (newPassword !== confirmPassword) {
      toast.info("Passwords do not match", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      await ChangePassword(oldPassword, newPassword);
      // Handle the password reset logic here
      setConfirmPassword("");
      setNewPassword("");
      setOldPassword("");
      console.log("Password successfully changed!");
    }
  };

  return (
    <div style={style}>
      <Typography variant="h4" gutterBottom>
        Change Password
      </Typography>
      <TextField
        required
        id="existingPassword"
        label="Existing Password"
        variant="outlined"
        type={showPassword ? "text" : "password"}
        value={oldPassword}
        onChange={(e) => setOldPassword(e.target.value)}
        fullWidth
        style={{ marginBottom: 16 }}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton aria-label="toggle password visibility" onClick={handleClickShowPassword} edge="end">
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />

      <TextField
        required
        id="newPassword"
        label="New Password"
        variant="outlined"
        type={showPassword ? "text" : "password"}
        value={newPassword}
        onChange={(e) => setNewPassword(e.target.value)}
        fullWidth
        style={{ marginBottom: 16 }}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton aria-label="toggle password visibility" onClick={handleClickShowPassword} edge="end">
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      <TextField
        required
        id="confirmPassword"
        label="Confirm Password"
        variant="outlined"
        type={confirmShowPassword ? "text" : "password"}
        value={confirmPassword}
        onChange={(e) => setConfirmPassword(e.target.value)}
        fullWidth
        style={{ marginBottom: 16 }}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton aria-label="toggle password visibility" onClick={handleClickShowConfirmPassword} edge="end">
                {confirmShowPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      {/* {errorMessage && (
        <Typography color="error" style={{ marginBottom: 16 }}>
          {errorMessage}
        </Typography>
      )} */}
      <Button variant="contained" fullWidth onClick={handleSubmit}>
        Reset Password
      </Button>
    </div>
  );
}

export default UChangePassword;
