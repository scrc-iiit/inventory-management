import React, { useState, useEffect, useRef, useContext } from "react";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Badge from "@mui/material/Badge";
import { DateCalendar, DayCalendarSkeleton, PickersDay } from "@mui/x-date-pickers";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import Itemcontext from "../context/Items/Itemcontext";

// Styles
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: "#F3F3F3",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
  height: "100%", // Ensure the item takes up full height of the grid item
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  position: "relative",
}));

const Header = styled("div")(({ theme }) => ({
  position: "absolute",
  top: theme.spacing(1),
  left: theme.spacing(1),
  fontSize: "1rem",
  color: "#123462",
}));

const Number = styled(Typography)(({ theme }) => ({
  fontSize: "2rem",
  color: "#123462",
}));

const containerStyle = {
  width: "100%",
  height: "60vh",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

const gridItemStyle = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "space-between",
};

// function findDays(year, month) {
//   // Filter dates that match the input year and month
//   let filteredDates = dates.filter((date) => {
//     let [dateYear, dateMonth] = date.split("-");
//     return dateYear === year && dateMonth === month;
//   });

//   // Map to extract day part from each date
//   let days = filteredDates.map((date) => {
//     return parseInt(date.split("-")[2]); // Extract day and convert to integer
//   });

//   days.sort();

//   // Return the sorted array of days
//   return days;
// }
// UHome Component
function UHome() {
  const context = useContext(Itemcontext);
  const { uHomeData, UHomecount } = context;
  const requestAbortController = useRef(null);
  const [isLoading, setIsLoading] = useState(false);
  const [dates, setDates] = useState([]);
  const [highlightedDays, setHighlightedDays] = useState([]);

  function fakeFetch(date, { signal }) {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        const Month = date.month();
        const Year = date.year();

        console.log(findDays(Year, Month + 1));

        const daysToHighlight = [26];

        resolve({ daysToHighlight });
      }, 500);
      // console.log(date.month());
      signal.onabort = () => {
        clearTimeout(timeout);
        reject(new DOMException("aborted", "AbortError"));
      };
    });
  }

  // ServerDay Component
  function ServerDay(props) {
    const { highlightedDays = [], day, outsideCurrentMonth, ...other } = props;

    const isHighlighted = !outsideCurrentMonth && highlightedDays.includes(day.date());

    return (
      <Badge key={day.toString()} overlap="circular" badgeContent={isHighlighted ? "🟢" : undefined}>
        <PickersDay {...other} outsideCurrentMonth={outsideCurrentMonth} day={day} />
      </Badge>
    );
  }

  const fetchHighlightedDays = (date) => {
    const controller = new AbortController();
    fakeFetch(date, {
      signal: controller.signal,
    })
      .then(({ daysToHighlight }) => {
        setHighlightedDays(daysToHighlight);
        setIsLoading(false);
      })
      .catch((error) => {
        // Ignore abort errors
        if (error.name !== "AbortError") {
          throw error;
        }
      });

    requestAbortController.current = controller;
  };

  function findDays(year, month) {
    // Filter dates that match the input year and month
    console.log(dates);
    let filteredDates = dates.filter((date) => {
      let [dateYear, dateMonth] = date.split("-");
      return dateYear === year && dateMonth === month;
    });

    // Map to extract day part from each date
    let days = filteredDates.map((date) => {
      return parseInt(date.split("-")[2]); // Extract day and convert to integer
    });

    days.sort();

    // Return the sorted array of days
    return days;
  }

  useEffect(() => {
    async function fetchStockData() {
      await UHomecount();
      setDates(uHomeData.dates);
    }
    fetchStockData();
    fetchHighlightedDays(dayjs()); // Use current date
    return () => requestAbortController.current?.abort();
    // eslint-disable-next-line
  }, []);

  const handleMonthChange = (date) => {
    if (requestAbortController.current) {
      requestAbortController.current.abort();
    }

    setIsLoading(true);
    setHighlightedDays([]);
    fetchHighlightedDays(date);
  };

  // console.log(uHomeData);
  return (
    <Box sx={containerStyle}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} style={gridItemStyle}>
          <Grid container direction="column" spacing={2} style={{ height: "28vw" }}>
            <Grid item xs>
              <Item>
                <Header>Total Requests</Header>
                <Number>{uHomeData.TotalCount}</Number>
              </Item>
            </Grid>
            <Grid item xs>
              <Item>
                <Header>Pending Requests</Header>
                <Number>{uHomeData.pendingCount}</Number>
              </Item>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Item style={{ height: "27vw" }}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DateCalendar
                readOnly
                loading={isLoading}
                onMonthChange={handleMonthChange}
                renderLoading={() => <DayCalendarSkeleton />}
                slots={{
                  day: ServerDay,
                }}
                slotProps={{
                  day: {
                    highlightedDays,
                  },
                }}
              />
            </LocalizationProvider>
          </Item>
        </Grid>
      </Grid>
    </Box>
  );
}

export default UHome;
