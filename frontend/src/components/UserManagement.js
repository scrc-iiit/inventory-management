import React, { useContext, useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Box,
  Typography,
  TextField,
  Grid,
  Button,
  Modal,
  FormControl,
  Select,
  InputLabel,
  MenuItem,
} from "@mui/material";
import AuthContext from "../context/auth/AuthContext";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


const containerStyle = {
  width: "100%",
  height: "80vh",
  marginTop: "5%",
  padding: "2%",
  boxSizing: "border-box",
  color: "#123462",
};

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function Changepass(props) {
  const context = useContext(AuthContext);
  const { userUpdate } = context;
  const [role, setRole] = useState(props.user.role || "");
  const [password, setPassword] = useState("");
  const [cpassword, setCPassword] = useState("");

  useEffect(() => {
    setRole(props.user.role || "");
  }, [props.user]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (password === cpassword) {
      let r = role === props.user.role ? props.user.role : role;
      let res = await userUpdate(props.user._id, r, password);
      console.log(res);
    } else {
      // alert
      toast.warn("Error!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    setCPassword("");
    setPassword("");
    props.handleClose();
  };
  return (
    <Modal open={props.open} onClose={props.handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Update Password
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField fullWidth label="Password" variant="outlined" value={password} onChange={(e) => setPassword(e.target.value)} />
            </Grid>
            <Grid item xs={12}>
              <TextField fullWidth label="Confirm Password" variant="outlined" value={cpassword} onChange={(e) => setCPassword(e.target.value)} />
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth margin="normal">
                <InputLabel id="role-label">Role</InputLabel>
                <Select labelId="role-label" id="role" value={role} label="Role" onChange={(e) => setRole(e.target.value)}>
                  <MenuItem value="admin">Admin</MenuItem>
                  <MenuItem value="user">User</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Submit
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

function DeleteAccount(props) {
  const context = useContext(AuthContext);
  const { UserDel } = context;

  const handleSubmit = async (event) => {
    event.preventDefault();
    // Handle form submission logic here
    let res = await UserDel(props.user._id);
    console.log(res);

    // Close the modal
    props.handleClose();
  };
  return (
    <Modal open={props.open} onClose={props.handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" gutterBottom>
          Account delete Permanently
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary">
                Delete
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
}

function UserManagement() {
  const acontext = useContext(AuthContext);
  const { UserManage, usersdata } = acontext;

  const navigate = useNavigate();

  const [openpass, setOpenpass] = useState(false);
  const [selectedUser, setSelectedUser] = useState({});
  const [opendel, setOpendel] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const handleOpenpass = (user) => {
    setSelectedUser(user);
    setOpenpass(true);
  };
  const handleClosepass = () => setOpenpass(false);
  const handleClosedel = () => setOpendel(false);

  const handleOpendel = (user) => {
    setSelectedUser(user);
    setOpendel(true);
  };

  useEffect(() => {
    async function fetchUserData() {
      await UserManage();
    }
    fetchUserData();
    // eslint-disable-next-line
  }, []);

  const filteredData = usersdata.filter(
    (user) =>
      user.username.toLowerCase().includes(searchQuery.toLocaleLowerCase()) || user.role.toLowerCase().includes(searchQuery.toLocaleLowerCase())
  );
  return (
    <Box sx={containerStyle}>
      <Changepass open={openpass} handleClose={handleClosepass} user={selectedUser} />
      <DeleteAccount open={opendel} handleClose={handleClosedel} user={selectedUser} />
      <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
        <Typography component="h1" variant="h5" sx={{ fontWeight: "bold", marginRight: 2 }}>
          User Management
        </Typography>
        <Box display="flex" alignItems="center">
          <TextField
            variant="outlined"
            placeholder="Search..."
            size="small"
            sx={{ mr: 2 }}
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
          <Button
            variant="contained"
            color="primary"
            sx={{ mr: 2 }}
            onClick={() => {
              navigate("/assign");
            }}
          >
            Add New User
          </Button>
        </Box>
      </Box>
      <TableContainer component={Paper} sx={{ maxHeight: "35vw", marginTop: 4 }}>
        <Table stickyHeader aria-label="user table">
          <TableHead>
            <TableRow>
              <TableCell align="center" sx={{ bgcolor: "#373A40", color: "#EEEEEE", fontWeight: "bold" }}>
                User Name
              </TableCell>
              <TableCell align="center" sx={{ bgcolor: "#373A40", color: "#EEEEEE", fontWeight: "bold" }}>
                User Email
              </TableCell>
              <TableCell align="center" sx={{ bgcolor: "#373A40", color: "#EEEEEE", fontWeight: "bold" }}>
                User Role
              </TableCell>
              <TableCell align="center" sx={{ bgcolor: "#373A40", color: "#EEEEEE", fontWeight: "bold" }}>
                Date Enrolled
              </TableCell>
              <TableCell align="center" sx={{ bgcolor: "#373A40", color: "#EEEEEE", fontWeight: "bold" }}>
                Update
              </TableCell>
              {localStorage.getItem("role") === "sysadmin" ? (
                <TableCell align="center" sx={{ bgcolor: "#373A40", color: "#EEEEEE", fontWeight: "bold" }}>
                  Delete Account
                </TableCell>
              ) : (
                ""
              )}
            </TableRow>
          </TableHead>
          <TableBody sx={{ bgcolor: "#B4B4B8" }}>
            {filteredData.map((user, index) => (
              <TableRow key={index} sx={{ "&:hover": { bgcolor: "#C7C8CC" } }}>
                <TableCell align="center">{user.username}</TableCell>
                <TableCell align="center">{user.email}</TableCell>
                <TableCell align="center">{user.role}</TableCell>
                <TableCell align="center">{new Date(user.date).toLocaleDateString()}</TableCell>
                {user.role !== "sysadmin" ? (
                  <>
                    <TableCell align="center">
                      <Button variant="contained" color="primary" onClick={() => handleOpenpass(user)}>
                        Update
                      </Button>
                    </TableCell>
                    <TableCell align="center">
                      <Button variant="contained" sx={{ bgcolor: "#E4003A" }} onClick={() => handleOpendel(user)}>
                        Delete
                      </Button>
                    </TableCell>
                  </>
                ) : (
                  <>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center"></TableCell>
                  </>
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}

export default UserManagement;
