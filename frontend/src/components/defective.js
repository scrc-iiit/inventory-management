import React, { useEffect, useState, useContext, Suspense } from 'react';
import Itemcontext from "../context/Items/Itemcontext";
import ReactApexChart from 'react-apexcharts';
import { toast } from 'react-toastify';

const DefectiveItemsChart = () => {
  const { Defectiveitemsdata, defectiveitemsdata = [] } = useContext(Itemcontext);
  const [chartConfig, setChartConfig] = useState({
    series: [],
    options: {
      chart: {
        type: 'bar',
        height: 350,
        stacked: true,
        toolbar: {
          show: false,
        },
        zoom: {
          enabled: true,
        },
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: 0,
              offsetY: 0,
            },
          },
        },
      ],
      plotOptions: {
        bar: {
          horizontal: false,
          borderRadius: 10,
          borderRadiusApplication: 'end',
          borderRadiusWhenStacked: 'last',
          dataLabels: {
            total: {
              enabled: true,
              style: {
                fontSize: '13px',
                fontWeight: 900,
              },
            },
          },
        },
      },
      xaxis: {
        type: 'datetime',
        categories: [],
      },
      legend: {
        position: 'top',
        horizontalAlign: 'center',
        offsetY: -1,
      },
      tooltip: {
        enabled: true,
        shared: false,
        intersect: true,
        followCursor: true,
        theme: 'dark',
        style: {
          fontSize: '14px',
        },
        x: {
          show: true,
          formatter: (value) => new Date(value).toLocaleDateString(),
        },
        y: {
          formatter: (value, { seriesIndex, dataPointIndex, w }) => {
            const seriesName = w.config.series[seriesIndex].name;
            return `${seriesName}: ${value}`;
          },
        },
      },
      fill: {
        opacity: 1,
      },
    },
  });

  const [interval, setIntervalValue] = useState('daily');
  const [loading, setLoading] = useState(true);
  const [autoFetch, setAutoFetch] = useState(false); // To control auto-fetch after first load

  const handleIntervalChange = (e) => {
    setIntervalValue(e.target.value);
  };

  const fetchDefectiveItemsData = async () => {
    try {
      setLoading(true); // Set loading only for initial fetch
      await Defectiveitemsdata();
    } catch (error) {
      console.error(error);
      toast.error('Failed to fetch defective items for chart display');
    } finally {
      setLoading(false); // Stop loading
    }
  };

  useEffect(() => {
    const initializeDataFetch = async () => {
      await fetchDefectiveItemsData();
      setAutoFetch(true); // Enable auto-fetch after initial load
    };

    initializeDataFetch();

    if (autoFetch) {
      const intervalId = setInterval(() => {
        fetchDefectiveItemsData(); // Fetch data every 3 minutes
      }, 3 * 60 * 1000); // 3 minutes

      return () => clearInterval(intervalId); // Clear interval on unmount
    }
  }, [autoFetch]);

  const getFilteredDate = (date) => {
    const currentDate = new Date(date);
    switch (interval) {
      case "daily":
        return currentDate.toISOString().split("T")[0];
      case "weekly":
        const startOfWeek = new Date(currentDate);
        startOfWeek.setDate(currentDate.getDate() - currentDate.getDay());
        return startOfWeek.toISOString().split("T")[0];
      case "monthly":
        return currentDate.toISOString().slice(0, 7);
      case "yearly":
        return currentDate.getFullYear().toString();
      default:
        return currentDate.toISOString().split("T")[0];
    }
  };

  useEffect(() => {
    const transformDataForChart = (data) => {
      if (!Array.isArray(data) || data.length === 0) return;

      const uniqueNames = [...new Set(data.map((item) => item.name))];
      const categories = [...new Set(data.map((item) => getFilteredDate(item.date)))];

      const seriesData = uniqueNames.map((name) => ({
        name,
        data: categories.map((category) => {
          const item = data.find(
            (entry) => entry.name === name && getFilteredDate(entry.date) === category
          );
          return item ? item.quantity : 0;
        }),
      }));

      setChartConfig((prevConfig) => ({
        ...prevConfig,
        series: seriesData,
        options: {
          ...prevConfig.options,
          xaxis: {
            ...prevConfig.options.xaxis,
            categories: categories.map((date) => new Date(date).getTime()),
          },
        },
      }));
    };

    transformDataForChart(defectiveitemsdata);
  }, [defectiveitemsdata, interval]);

  return (
    <div style={{ marginTop: '2vw' }}>
      <select
        onChange={handleIntervalChange}
        value={interval}
        style={{ marginLeft: '18vw', marginBottom: '1.5vw' }}
      >
        <option value="daily">Daily</option>
        <option value="weekly">Weekly</option>
        <option value="monthly">Monthly</option>
        <option value="yearly">Yearly</option>
      </select>

      <Suspense fallback={<p>Loading chart...</p>}>
        {loading ? (
          <p>Loading data...</p>
        ) : chartConfig.series.length > 0 ? (
          <ReactApexChart
            options={chartConfig.options}
            series={chartConfig.series}
            type="bar"
            height={290}
          />
        ) : (
          <p>No data available to display the chart.</p>
        )}
      </Suspense>
    </div>
  );
};

export default DefectiveItemsChart;
