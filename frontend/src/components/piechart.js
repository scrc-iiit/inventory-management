import React, { useContext, useEffect, useState, Suspense } from "react";
import Chart from "react-apexcharts";
import ItemContext from "../context/Items/Itemcontext";
import dayjs from "dayjs";
import weekOfYear from "dayjs/plugin/weekOfYear"; // Import the weekOfYear plugin

// Extend dayjs with the weekOfYear plugin for weekly operations
dayjs.extend(weekOfYear);

const PieChart = () => {
  const { RequestList, requestData } = useContext(ItemContext);
  const [approvedData, setApprovedData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [timeFilter, setTimeFilter] = useState("daily");
  const [loading, setLoading] = useState(true);

  // Fetch data and handle lazy loading
  const fetchApprovedData = async () => {
    setLoading(true); // Show loading indicator
    try {
      await RequestList(); // Fetch data from the endpoint
      setLoading(false); // Hide loading indicator after data is fetched
    } catch (error) {
      console.error("Error fetching data:", error);
      setLoading(false); // Hide loading indicator even if there's an error
    }
  };

  // Initial fetch and interval setup
  useEffect(() => {
    fetchApprovedData();

    // Set up a 10-minute interval for fetching data
    const intervalId = setInterval(() => {
      fetchApprovedData();
    }, 5 * 60 * 1000); // 10 minutes

    return () => clearInterval(intervalId); // Clear interval on component unmount
  }, []);

  // Filter approved data
  useEffect(() => {
    if (requestData && Array.isArray(requestData)) {
      const approvedItems = requestData.filter((item) => item.status === "Approved");
      setApprovedData(approvedItems);
    } else {
      console.warn("requestData is empty or not an array");
      setApprovedData([]);
    }
  }, [requestData]);

  // Apply time-based filtering
  useEffect(() => {
    const filterDataByTime = () => {
      const now = dayjs();
      let filtered;

      switch (timeFilter) {
        case "daily":
          const dailyData = approvedData.reduce((acc, item) => {
            const itemDate = item.date ? dayjs(item.date).format("YYYY-MM-DD") : "No Date";
            if (!acc[itemDate]) {
              acc[itemDate] = { ...item, usedQuantity: parseInt(item.usedQuantity, 10) || 0 };
            } else {
              acc[itemDate].usedQuantity += parseInt(item.usedQuantity, 10) || 0;
            }
            return acc;
          }, {});
          filtered = Object.values(dailyData);
          break;
        case "weekly":
          const currentWeek = now.week();
          filtered = approvedData.filter((item) => {
            const itemWeek = item.date ? dayjs(item.date).week() : null;
            return itemWeek === currentWeek || itemWeek === currentWeek - 1;
          });
          break;
        case "monthly":
          filtered = approvedData.filter((item) =>
            item.date && dayjs(item.date).isSame(now, "month")
          );
          break;
        case "yearly":
          filtered = approvedData.filter((item) =>
            item.date && dayjs(item.date).isSame(now, "year")
          );
          break;
        default:
          filtered = approvedData;
      }
      setFilteredData(filtered);
    };

    filterDataByTime();
  }, [timeFilter, approvedData]);

  // Prepare data for the Pie Chart
  const series = filteredData.map((item) => {
    const quantity = parseInt(item.usedQuantity, 10);
    return isNaN(quantity) ? 0 : quantity;
  });

  const labels = filteredData.map((item) => item.name || "Unnamed");

  const options = {
    title: {
      text: `Approved Items - ${timeFilter.charAt(0).toUpperCase() + timeFilter.slice(1)}`,
      align: "center",
      style: {
        fontSize: "18px",
        fontWeight: "bold",
      },
    },
    labels,
    chart: {
      type: "pie",
      height: 400,
    },
    legend: {
      position: "bottom",
    },
    tooltip: {
      enabled: true,
      followCursor: true,
      y: {
        formatter: (val, opts) => {
          const item = filteredData[opts.seriesIndex];
          const itemDate = item && item.date ? dayjs(item.date).format("YYYY-MM-DD") : "No Date";
          return `${val} items on ${itemDate}`;
        },
      },
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 300,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <div>
      <div>
        <select
          id="timeFilter"
          value={timeFilter}
          onChange={(e) => setTimeFilter(e.target.value)}
          style={{ marginLeft: "18vw", marginBottom: "2vw" }}
        >
          <option value="daily">Daily</option>
          <option value="weekly">Weekly</option>
          <option value="monthly">Monthly</option>
          <option value="yearly">Yearly</option>
        </select>
      </div>
      <Suspense fallback={<p>Loading chart...</p>}>
        {loading ? (
          <p>Loading data...</p>
        ) : filteredData.length > 0 ? (
          <Chart options={options} series={series} type="pie" width="320" />
        ) : (
          <p>No approved items to display</p>
        )}
      </Suspense>
    </div>
  );
};

export default PieChart;
