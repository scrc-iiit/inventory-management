import React, { useContext, useState, useEffect } from "react";
import ReactApexChart from "react-apexcharts";
import ItemContext from "../context/Items/Itemcontext";

const StackedBarChartWithData = () => {
  const { StockList, stockList } = useContext(ItemContext); // Fetch stock data from context
  const [interval, setInterval] = useState("daily"); // Default interval: daily
  const [loading, setLoading] = useState(true); // Loading state
  const [chartData, setChartData] = useState({ series: [], categories: [] }); // Chart data state

  const formatDate = (date, intervalType) => {
    switch (intervalType) {
      case "daily":
        return date.toISOString().split("T")[0]; // YYYY-MM-DD
      case "weekly":
        const startOfWeek = new Date(date);
        startOfWeek.setDate(date.getDate() - date.getDay());
        return startOfWeek.toISOString().split("T")[0]; // Start of the week
      case "monthly":
        return date.toISOString().slice(0, 7); // YYYY-MM
      case "yearly":
        return date.getFullYear().toString(); // YYYY
      default:
        return date.toISOString().split("T")[0];
    }
  };

  const processData = () => {
    const categories = new Set();
    const seriesData = {};

    stockList.forEach((stock) => {
      if (stock.createdAt && stock.name && typeof stock.stocks === "number") {
        const date = new Date(stock.createdAt);
        const category = formatDate(date, interval);

        categories.add(category);

        if (!seriesData[stock.name]) {
          seriesData[stock.name] = {};
        }

        if (!seriesData[stock.name][category]) {
          seriesData[stock.name][category] = 0;
        }

        seriesData[stock.name][category] += stock.stocks;
      }
    });

    const sortedCategories = Array.from(categories).sort();
    const series = Object.keys(seriesData).map((name) => {
      const data = sortedCategories.map((category) => seriesData[name][category] || 0);
      return { name, data };
    });

    setChartData({
      series,
      categories: sortedCategories,
    });
    setLoading(false);
  };

  const fetchData = async () => {
    setLoading(true);
    await StockList(); // Fetch stock list data from the context
    processData(); // Process the data
  };

  useEffect(() => {
    fetchData(); // Fetch and display daily data by default on component mount
  }, []); // Only run once on mount

  useEffect(() => {
    processData(); // Re-process data when the interval changes
  }, [interval, stockList]); // Dependencies include interval and stockList

  const handleIntervalChange = (e) => {
    setInterval(e.target.value); // Update interval state
  };

  const options = {
    chart: { type: "bar", height: 350, stacked: true },
    plotOptions: {
      bar: {
        horizontal: true,
        dataLabels: { total: { enabled: true, style: { fontSize: "13px", fontWeight: 900 } } },
      },
    },
    stroke: { width: 1, colors: ["#fff"] },
    xaxis: { categories: chartData.categories.length ? chartData.categories : ["No data available"] },
    tooltip: { y: { formatter: (val) => `${val}` } },
    fill: { opacity: 1 },
    legend: { position: "top", horizontalAlign: "left", offsetX: 20 },
  };

  return (
    <div>
      <select
        onChange={handleIntervalChange}
        value={interval}
        style={{ marginLeft: "18vw", marginTop: "1.5vw" }}
      >
        <option value="daily">Daily</option>
        <option value="weekly">Weekly</option>
        <option value="monthly">Monthly</option>
        <option value="yearly">Yearly</option>
      </select>

      {loading ? (
        <p>Loading data...</p> // Show loading message until data is processed
      ) : chartData.series.length > 0 ? (
        <ReactApexChart
          options={options}
          series={chartData.series}
          type="bar"
          height={310}
        />
      ) : (
        <p>No data available</p> // Show "no data" message if series is empty
      )}
    </div>
  );
};

export default StackedBarChartWithData;
