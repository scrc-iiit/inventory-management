import React, { useState, useEffect,useRef } from "react";
import Itemcontext from "./Itemcontext";
import config from "../../config";
import { axiosInstance, axiosAuthInstance } from '../../services/axiosConfig';
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Itemstate(props) {
  const host = config.API_BASE_URL;

  // States
  const [datastocklog, setDatastocklog] = useState([]);
  const [defectivedata, setDefectivedata] = useState([]);
  const [defectiveitemsdata, setDefectiveitemsdata] = useState([]);
  const [stockList, setStockList] = useState([]);
  const [requestData, setRequestData] = useState([]);
  const [uHomeData, setUHomeData] = useState([]);
  const [aHomeData, setAHomeData] = useState(null);
  const [notificationCount, setNotificationCount] = useState(0);
  const [notifications, setNotifications] = useState([]);
  const [alarmCount, setAlarmCount] = useState(0);
  const [alarms, setAlarms] = useState([]);
  const lastFetchTime = useRef(null);

  // Function to fetch data at regular intervals
  const fetchDataAtInterval = (apiFunction, stateSetter, interval = 300000) => {
    const intervalId = setInterval(async () => {
      try {
        await apiFunction(stateSetter);
      } catch (error) {
        console.error("Error fetching data:", error);
        toast.error("Failed to fetch data", { position: "top-right" });
      }
    }, interval);
    return intervalId;
  };

  // Fetch admin home count
  const AHomecount = async () => {
    try {
      const response = await axiosAuthInstance.get(`/api/stock/count`);
      setAHomeData(response.data);
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch home count", { position: "top-right" });
    }
  };

  // Buy stock
  const BuyStock = async (sku, remarks, quantity) => {
    try {
      const response = await axiosAuthInstance.post(`/api/store/use/create`, {
        sku,
        purpose: remarks,
        usedQuantity: quantity,
      });

      if (response.data.status === true) {
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg, { position: "top-right" });
      }

      setRequestData(requestData.concat(response.data.savedUsedItem));
      return response.data;
    } catch (error) {
      console.error(error);
      toast.error("Failed to buy stock", { position: "top-right" });
    }
  };

  // Fetch stock list
  const StockList = async () => {
    try {
      const response = await axiosAuthInstance.get(`/api/stock/getall`);
      setStockList(response.data);
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch stock list", { position: "top-right" });
    }
  };

  // Add new stock
  const AddNewStock = async (name, location) => {
    try {
      const response = await axiosAuthInstance.post(`/api/stock/generate`, { name, location });

      if (response.data.status === true) {
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg, { position: "top-right" });
      }

      setStockList(stockList.concat(response.data.savedstock));
      return response.data;
    } catch (error) {
      console.error(error);
      toast.error("Failed to add new stock", { position: "top-right" });
    }
  };

  // Add purchased stock
  const AddBuyStock = async (sku, quantity, purchaseDate, storeName, price, purpose) => {
    try {
      const response = await axiosAuthInstance.post(`/api/store/purchase/add`, {
        sku,
        quantity,
        purchase_date: purchaseDate,
        price: Number(price),
        store_name: storeName,
        purpose: purpose,
      });

      if (response.data.status === true) {
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg, { position: "top-right" });
      }

      const updatedData = stockList.map((item) => {
        if (item.SKU === sku) {
          return { ...item, stocks: response.data.Updatestock.stocks };
        }
        return item;
      });

      setDatastocklog(datastocklog.concat(response.data.savedItem));
      setStockList(updatedData);
      return response.data;
    } catch (error) {
      console.error(error);
      toast.error("Failed to add buy stock", { position: "top-right" });
    }
  };

  // Fetch stock log
  const Stocklog = async () => {
    try {
      const response = await axiosAuthInstance.get(`/api/store/purchase/getall`);
      setDatastocklog(response.data);
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch stock log", { position: "top-right" });
    }
  };

  // Delete stock log
  const StocklogDel = async (id) => {
    try {
      const response = await axiosAuthInstance.delete(`/api/store/purchase/delete/${id}`);
      if (response.data.status === true) {
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg, { position: "top-right" });
      }
      const updatedStockLog = datastocklog.filter((row) => row._id !== id);
      setDatastocklog(updatedStockLog);
    } catch (error) {
      console.error(error);
      toast.error("Failed to delete stock log", { position: "top-right" });
    }
  };

  // Fetch request list
  const RequestList = async () => {
    try {
      const response = await axiosAuthInstance.get(`/api/store/use/getall`);
      setRequestData(response.data);
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch request list", { position: "top-right" });
    }
  };

  

  // Delete request
  const RequestDel = async (id) => {
    try {
      const response = await axiosAuthInstance.delete(`/api/store/use/delete/${id}`);
      if (response.data.status === true) {
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg, { position: "top-right" });
      }
      const updatedRequestData = requestData.filter((row) => row._id !== id);
      setRequestData(updatedRequestData);
    } catch (error) {
      console.error(error);
      toast.error("Failed to delete request", { position: "top-right" });
    }
  };

  // Update request status
  const ReqUpdate = async (id, status, remarks = '') => {
    try {
      const payload = { status };
      if (status === 'Rejected') {
        payload.remarks = remarks; // Add remarks only for rejection
      }

      const response = await axiosAuthInstance.put(`/api/store/use/update/${id}`, payload);

      if (response.data.status === true) {
        const savedUsedItem = response.data.savedUsedItem;

        const newReq = requestData.map((item) =>
          item._id === id
            ? {
                ...item,
                status: status,
                Reported_by: savedUsedItem.Reported_by || '',
                rejectionRemarks: status === 'Rejected' ? savedUsedItem.rejectionRemarks || '' : '',
              }
            : item
        );
        setRequestData(newReq);
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg || 'Update failed, please try again.', { position: "top-right" });
      }
    } catch (error) {
      console.error('Error updating request:', error);
      if (error.response) {
        const errorMsg = error.response.data.msg || 'Failed to update request. Please try again.';
        toast.error(errorMsg, { position: "top-right" });
      } else {
        toast.error("Failed to update request. Please try again.", { position: "top-right" });
      }
    }
  };

  // Fetch user home count
  const UHomecount = async () => {
    try {
      const response = await axiosAuthInstance.get(`/api/store/use/unique-dates`);
      setUHomeData(response.data);
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch user home count", { position: "top-right" });
    }
  };

  // Fetch defective items
  const Defectiveitems = async () => {
    try {
      const response = await axiosAuthInstance.get(`/api/damaged/getall`);
      setDefectivedata(response.data);
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch defective items", { position: "top-right" });
    }
  };

  const Defectiveitemsdata = async () => {
    try {
      const response = await axiosAuthInstance.get(`/api/damaged/getall`);
  
      // Check if response data is an array
      if (Array.isArray(response.data)) {
        const formattedData = response.data.map(item => ({
          name: item.name || "Unknown", // Handle missing fields gracefully
          quantity: item.quantity || 0,
          date: item.date || new Date().toISOString(), // Default to current date if missing
        }));
        setDefectiveitemsdata(formattedData);
      } else {
        console.error("Unexpected response format:", response.data);
        toast.error("Failed to process defective items data", { position: "top-right" });
      }
    } catch (error) {
      console.error("Error fetching defective items:", error.response?.data || error.message);
      toast.error("Failed to fetch defective items", { position: "top-right" });
    }
  };
  

  // Delete defective item
  const DefectiveDel = async (id) => {
    try {
      const response = await axiosAuthInstance.delete(`/api/damaged/delete/${id}`);
      if (response.data.status === true) {
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg, { position: "top-right" });
      }
      const updatedDefectiveData = defectivedata.filter((row) => row._id !== id);
      setDefectivedata(updatedDefectiveData);
    } catch (error) {
      console.error(error);
      toast.error("Failed to delete defective item", { position: "top-right" });
    }
  };

  // Add defective item
  const AddDefective = async (name, quantity, cause, verified) => {
    try {
      const response = await axiosAuthInstance.post(`/api/damaged/request`, {
        name,
        quantity,
        cause,
        verify: verified,  // Ensure this matches what the backend expects
      });
  
      // Check if the request was successful
      if (response.data.status === true) {
        toast.success(response.data.msg, { position: "top-right" });
  
        // Add new data to the defectivedata state, assuming `savedDRS` is correct
        setDefectivedata(defectivedata.concat(response.data.savedDRS));
      } else {
        // Display the error message from the server response
        toast.error(response.data.msg, { position: "top-right" });
      }
  
      return response.data;
    } catch (error) {
      console.error("Error adding defective item:", error.response ? error.response.data : error.message);
      toast.error("Failed to add defective item", { position: "top-right" });
    }
  };
  

  // Fetch notification count (stage "N")
  const fetchNotificationCount = async () => {
    try {
      const now = new Date().getTime(); // Current time in milliseconds
      if (!lastFetchTime.current || now - lastFetchTime.current >= 5 * 60 * 1000) {
      const response = await axiosAuthInstance.get(`/api/alarm/notification/count`);
      setNotificationCount(response.data.count);
      }
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch notification count", { position: "top-right" });
    }
  };

  // Fetch notification data (stage "N")
  const fetchNotifications = async () => {
    try {
      const now = new Date().getTime(); // Current time in milliseconds
      if (!lastFetchTime.current || now - lastFetchTime.current >= 5 * 60 * 1000) {
      const response = await axiosAuthInstance.get(`/api/alarm/notification/data`);
      if (response.data.success) {
        setNotifications(response.data.notifications);
      }
      }
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch notification data", { position: "top-right" });
    }
  };

  // Fetch alarm count (stage "A")
  const fetchAlarmCount = async () => {
    try {
      const now = new Date().getTime(); // Current time in milliseconds
      if (!lastFetchTime.current || now - lastFetchTime.current >= 5 * 60 * 1000) {
      const response = await axiosAuthInstance.get(`/api/alarm/alarm/count`);
      setAlarmCount(response.data.count);
      }
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch alarm count", { position: "top-right" });
    }
  };

  // Fetch alarm data (stage "A")
  // const fetchAlarms = async () => {
  //   try {
  //     const response = await axiosAuthInstance.get(`/api/alarm/alarm/data`);
  //     if (response.data.success) {
  //       setAlarms(response.data.alarms);
  //     }
  //   } catch (error) {
  //     console.error(error);
  //     toast.error("Failed to fetch alarm data", { position: "top-right" });
  //   }
  // };

  const fetchAlarms = async () => {
     // Stores the last fetch timestamp
  
    try {
      const now = new Date().getTime(); // Current time in milliseconds
  
      // Check if it's been at least 5 minutes since the last fetch
      if (!lastFetchTime.current || now - lastFetchTime.current >= 5 * 60 * 1000) {
        const response = await axiosAuthInstance.get(`/api/alarm/alarm/data`);
        if (response.data.success) {
          setAlarms(response.data.alarms);
          lastFetchTime.current = now; // Update the last fetch time
        }
      } else {
        console.log("Fetch skipped: Less than 5 minutes since last fetch");
      }
    } catch (error) {
      console.error(error);
      toast.error("Failed to fetch alarm data", { position: "top-right" });
    }
  };

   // Update request data (new function)
   const updateRequestData = async (id, updatedData) => {
    try {
      const response = await axiosAuthInstance.put(`/api/store/use/update/${id}`, updatedData);
      if (response.data.status === true) {
        const updatedRequests = requestData.map((item) =>
          item._id === id ? { ...item, ...updatedData } : item
        );
        setRequestData(updatedRequests);
        toast.success(response.data.msg, { position: "top-right" });
      } else {
        toast.error(response.data.msg, { position: "top-right" });
      }
    } catch (error) {
      console.error("Error updating request data:", error);
      toast.error("Failed to update request data", { position: "top-right" });
    }
  };

  // Mark notification as read
  const markNotificationAsRead = async (id) => {
    try {
      const response = await axiosAuthInstance.put(`/api/alarm/notification/mark-as-read/${id}`);
      if (response.data.success) {
        const updatedNotifications = notifications.map((notification) =>
          notification._id === id ? { ...notification, status: "S" } : notification
        );
        setNotifications(updatedNotifications);
        toast.success(response.data.message, { position: "top-right" });
      } else {
        toast.error("Failed to mark notification as read", { position: "top-right" });
      }
    } catch (error) {
      console.error("Error marking notification as read:", error);
      toast.error("Failed to mark notification as read", { position: "top-right" });
    }
  };

  // Mark alarm as responded
  const markAlarmAsResponded = async (id) => {
    try {
      const response = await axiosAuthInstance.put(`/api/alarm/mark-as-responded/${id}`);
      if (response.data.success) {
        const updatedAlarms = alarms.map((alarm) =>
          alarm._id === id ? { ...alarm, status: "S" } : alarm
        );
        setAlarms(updatedAlarms);
        toast.success(response.data.message, { position: "top-right" });
      } else {
        toast.error("Failed to mark alarm as responded", { position: "top-right" });
      }
    } catch (error) {
      console.error("Error marking alarm as responded:", error);
      toast.error("Failed to mark alarm as responded", { position: "top-right" });
    }
  };


   // Component mount
   useEffect(() => {
    // Start fetching data for all `getall` endpoints
    const stocklogInterval = fetchDataAtInterval(Stocklog, setDatastocklog, 300000);
    const defectivedataInterval = fetchDataAtInterval(Defectiveitems, setDefectivedata, 300000);
    const stockListInterval = fetchDataAtInterval(StockList, setStockList);
    const requestListInterval = fetchDataAtInterval(RequestList, setRequestData);
    const defectivedataitemsinterval = fetchDataAtInterval(Defectiveitemsdata, setDefectiveitemsdata);
    const alarmCountInterval = fetchDataAtInterval(fetchAlarmCount, setAlarmCount, 300000);
    const alarmDataInterval = fetchDataAtInterval(fetchAlarms, setAlarms, 300000);
    const notificationcountinterval = fetchDataAtInterval(fetchNotificationCount, setNotificationCount, 300000);
    const notificationdatainterval = fetchDataAtInterval(fetchNotifications, setNotifications, 300000);
  

        // Clear intervals on component unmount
        return () => {
          clearInterval(stocklogInterval);
          clearInterval(defectivedataInterval);
          clearInterval(stockListInterval);
          clearInterval(requestListInterval);
          clearInterval(defectivedataitemsinterval);
          clearInterval(alarmCountInterval);
          clearInterval(alarmDataInterval);
          clearInterval(notificationcountinterval);
          clearInterval(notificationdatainterval)
         
        };
      }, []);

  return (
    <Itemcontext.Provider
      value={{
        AHomecount,
        aHomeData,
        Stocklog,
        datastocklog,
        StocklogDel,
        StockList,
        stockList,
        BuyStock,
        AddNewStock,
        AddBuyStock,
        RequestList,
        requestData,
        RequestDel,
        ReqUpdate,
        uHomeData,
        UHomecount,
        defectivedata,
        defectiveitemsdata,
        Defectiveitems,
        Defectiveitemsdata,
        DefectiveDel,
        AddDefective,
        fetchNotificationCount,
        notificationCount,
        fetchNotifications,
        notifications,
        fetchAlarmCount,
        alarmCount,
        fetchAlarms,
        alarms,
        markNotificationAsRead,
        markAlarmAsResponded,
        updateRequestData, // Include the new function here

      }}
    >
      {props.children}
    </Itemcontext.Provider>
  );
}

export default Itemstate;
