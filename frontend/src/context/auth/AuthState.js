import React, { useState } from "react";
import { axiosInstance, axiosAuthInstance, } from '../../services/axiosConfig';
import AuthContext from "./AuthContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function AuthState(props) {
  const [usersdata, setUserdata] = useState([]);

  const login = async (username, password) => {
    const response = await axiosInstance.post('/api/auth/login', {
      username,
      password,
    });
    const json = response.data;
    if (json.status) {
      localStorage.setItem("token", json.authtoken);
    }
    if (json.status === true) {
      toast.success(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    return json;
  };

  const logout = () => {
    localStorage.clear("token");
    localStorage.clear("role");
  };

  const UserDel = async (id) => {
    const response = await axiosAuthInstance.delete(`/api/auth/dprofile/${id}`);
    const json = response.data;
    const denote = usersdata.filter((user) => user._id !== id);
    if (json.status === true) {
      toast.success(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    setUserdata(denote);
  };

  const userUpdate = async (id, role, password) => {
    const response = await axiosAuthInstance.put(`/api/auth/uprofile/${id}`, {
      role,
      password,
    });
    const json = response.data;
    const newUser = JSON.parse(JSON.stringify(usersdata));
    for (let index = 0; index < usersdata.length; index++) {
      const element = newUser[index];
      if (element._id === id) {
        newUser[index].role = role;
        break;
      }
    }
    if (json.status === true) {
      toast.success(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    setUserdata(newUser);
  };

  const getUser = async () => {
    const response = await axiosAuthInstance.get('/api/auth/getuser');
    const json = response.data;
    localStorage.setItem("role", json.role);
    return json;
  };

  const UserManage = async () => {
    const response = await axiosAuthInstance.get('/api/auth/getalluser');
    const json = response.data;
    if (json.status === false) {
      return json.msg;
    }
    setUserdata(json);
  };

  const ChangePassword = async (oldPassword, newPassword) => {
    const response = await axiosAuthInstance.put('/api/auth/changepassword', {
      oldPassword,
      newPassword,
    });
    const json = response.data;
    if (json.status === true) {
      toast.success(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const Signup = async (username, email, password, role) => {
    const response = await axiosInstance.post('/api/auth/createcredentials', {
      username,
      email,
      password,
      role,
    });
    const json = response.data;
    if (json.status === true) {
      toast.success(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error(json.msg, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    return json;
  };

  return (
    <AuthContext.Provider
      value={{
        login,
        logout,
        getUser,
        userUpdate,
        UserManage,
        usersdata,
        UserDel,
        ChangePassword,
        Signup,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
}

export default AuthState;
