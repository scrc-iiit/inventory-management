import axios from 'axios';

let axiosInstance;
let axiosAuthInstance;
let BACKEND_API_URL;

const fetchConfig = async () => {
  try {
    const response = await fetch(`${process.env.PUBLIC_URL}/backend.json`);
    const data = await response.json();
    BACKEND_API_URL = data.BACKEND_API_URL;
  } catch (error) {
    console.error('Error fetching BACKEND_API_URL:', error);
  }

  axiosInstance = axios.create({
    baseURL: BACKEND_API_URL,
    timeout: 10000,
    headers: { 'Content-Type': 'application/json' },
  });

  axiosAuthInstance = axios.create({
    baseURL: BACKEND_API_URL,
    timeout: 10000,
    headers: { 'Content-Type': 'application/json' },
  });

  axiosAuthInstance.interceptors.request.use(async (config) => {
    const accessToken = localStorage.getItem("token");
    if (accessToken) {
      return {
        ...config,
        headers: { ...config.headers, 'auth-token': `${accessToken}` },
      };
    }
    return config;
  });
};

const isAxiosReady = fetchConfig();

export { axiosInstance, axiosAuthInstance, BACKEND_API_URL, isAxiosReady };
