#!/bin/bash

if [ -n "$BACKEND_API_URL" ]; then
    echo "{\"BACKEND_API_URL\": \"$BACKEND_API_URL\"}" > /usr/share/nginx/html/backend.json
fi

nginx -g 'daemon off;'